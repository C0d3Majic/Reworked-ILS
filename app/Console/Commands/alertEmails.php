<?php

namespace App\Console\Commands;

use App\Models\EmailQueue;
use App\Models\AlertsModel;
use App\Models\Log;
use App\User;
use App\Models\Lines;
use App\Models\Machines;
use App\Models\Codes;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class alertEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:alerts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is the command for sending alerts upon their escalation level';

    protected $info = 'Run was succesfully';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time_now = Carbon::now('America/Chihuahua');
        if($time_now->hour==12 && $time_now->minute==0)
        {
            $tomorrow = Carbon::now('America/Chihuahua')->addDays(1);
            $birthday_alerts = DB::select('select * from alerts where DAY(birthdate) = :d and MONTH (birthdate) = :m', ['m' => $tomorrow->month, 'd' => $tomorrow->day]);
            foreach ($birthday_alerts as $email_alert) {
                $to = $email_alert->email.', nuvocantera@hotmail.com';
                $subject = 'Nuvolisso te desea un feliz cumpleanos';
                $message = 'Estimado(a): ' . $email_alert->name . ', en Nuvolisso sabemos que manana es tu cumpleanos asi que te enviamos una cordial felicitacion y te queremos obsequiar un alasiado totalmente gratis (promocion unicamente el dia de tu cumpleanos), llamanos a cualquiera de nuestras sucursales para agendar tu cita, tels: 648 08 85 y 6 27 85 88';
                $headers = 'From: contacto@nuvolisso.com';
                mail($to, $subject, $message, $headers);
            }

            /*$alert = new AlertsModel();
            $alert->name = 'Cron '.Carbon::now('America/Chihuahua'). ', con correoooo';
            $alert->phone = '6561207320';
            $alert->visit_date = Carbon::now('America/Chihuahua');
            $alert->birthdate = Carbon::now('America/Chihuahua');
            $alert->email+- = 'angelsolares@gmail.com';//'count birthday = '.$birthday_alerts->count();
            $alert->attendant = $birthday_alerts->lenght();//'$time_now = '.$time_now;
            $alert->service = '$tomorrow!! = '.$tomorrow;
            $alert->observations = '$next_week month = '.$next_week->month;
            $alert->observations .= ' $next_week day = '.$next_week->day;
            $alert->save();*/

            $next_week = Carbon::now('America/Chihuahua')->addDays(7);
            $visit_alerts = DB::select('select * from alerts where DAY(visit_date) = :d and MONTH (visit_date) = :m', ['m' => $next_week->month, 'd' => $next_week->day]);
            foreach ($visit_alerts as $email_alert) {
                $to = $email_alert->email.', nuvocantera@hotmail.com';
                $subject = 'Correo de recordatorio de visita de Nuvolisso';
                $message = 'Estimado(a): ' . $email_alert->name . ', Nuvolisso agradece su preferencia y le recordamos que la fecha de su proxima visita esta cerca, llamenos a cualquiera de nuestras sucursales para agendar su cita, tels: 648 08 85 y 6 27 85 88';
                $headers = 'From: contacto@nuvolisso.com';
                mail($to, $subject, $message, $headers);
            }
        }
    }
}
