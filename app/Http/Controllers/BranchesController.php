<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\Languages;
use App\Models\Teachers;
use App\Models\Branches;
use App\User;

//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use View;

class BranchesController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branches::where('is_active',1)->get();
        return view('branches.index')->with('branches', $branches);
    }

    public function retrieve_branch(Request $request)
    {
        $branch_id = $request->input('branch_id');
        $branch = Branches::find($branch_id);
        
        $modal_data = '<h5>Deleting branch: <b>'. $branch->name .'</b></h5>' ;
        return response()->json(array('status' => 'success', 'modal' => $modal_data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // load the view and pass the nerds
        return View::make('branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'contact' => 'required'
        ]);

        $branch = new Branches();
        $branch->name = $request->input('name');
        $branch->address = $request->input('address');
        $branch->phone = $request->input('phone');
        $branch->contact = $request->input('contact');
        $branch->save();

        $this->saveLog($branch->id, $branch, 'new', 'branches');

        return Redirect::to('branches');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $course = Courses::find($id);
        $teachers = Teachers::all();
        $languages  = Languages::all();

        // load the view and pass the nerds
        return View::make('branches.show')
            ->with('course', $course)
            ->with('teachers', $teachers)
            ->with('languages',$languages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the nerds
        $branch = Branches::find($id);

        // load the view and pass the nerds
        return View::make('branches.edit')
            ->with('branch', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Branches::find($id);
        $this->saveLog($id, $branch, 'updateBefore', 'branches');

        $branch->name = $request->input('name');
        $branch->address = $request->input('address');
        $branch->phone = $request->input('phone');
        $branch->contact = $request->input('contact');
        $branch->save();

        $this->saveLog($id, $branch, 'updateAfter', 'branches');

        return Redirect::to('branches');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Branches = Branches::findOrFail($id);
        $this->saveLog($id, $Branches, 'delete', 'branches');
        $Branches->is_active = 0;
        $Branches->save();
        //$Course->delete();
        return "success";
    }
}
