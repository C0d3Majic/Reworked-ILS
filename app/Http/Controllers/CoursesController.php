<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\Languages;
use App\Models\Teachers;
use App\Models\Branches;
use App\User;

//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use View;

class CoursesController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 1){
            $courses = Courses::orderBy('id','desc')->where('branch_id',1)->where('is_active',1)->get();
            $branches = Branches::where('is_active',1)->get();
            return view('courses.index')
                ->with('courses', $courses)->with('branches', $branches);
        }else
        {
            $courses = Courses::orderBy('id','desc')->where('branch_id',Auth::user()->branch_id)->where('is_active',1)->get();

        return view('courses.index')
            ->with('courses', $courses);
        }
    }

    public function edit_description(Request $request)
    {
        $course = Courses::find($request->input('course_id'));
        $modal_data = '<h5>Editando la Nota del Curso : <b>'. $course->name .'</b></h5>' ;
        $modal_data .= '<div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">assignment</i>
                                <label for="notes" class="active">Nota</label>
                                <textarea id="notes" class="materialize-textarea">'.$course->notes.'</textarea>
                            </div>
                        </div>';
        $modal_data .= '<div class="row">
              <div class="col s6">
                <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar Cambios</a>
              </div>
              <div class="col s6">
                <a href="#!" id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar Cambios</a>
              </div>
            </div>';
        return response()->json(array('status' => 'success', 'modal' => $modal_data));
    }

    public function save_edit_description(Request $request)
    {
        $course_id = $request->input('course_id');
        $course = Courses::find($course_id);
        $course->notes = $request->input('notes');
        $course->save();
        return response()->json(array('status' => 'success'));
    }

    public function retrieve_course(Request $request)
    {
        $course_id = $request->input('course_id');
        $courses = Courses::find($course_id);
        
        $modal_data = '<h5>Deleting course: <b>'. $courses->name .'</b></h5>' ;
        return response()->json(array('status' => 'success', 'modal' => $modal_data));
    }

    public function change_branch(Request $request){
        $branch_id = $request->input('branch_id');
        $courses = Courses::orderBy('id', 'desc')->where('branch_id',$branch_id)->where('is_active',1)->get();
        foreach($courses as $course)
        {
            $course->language;
            $course->teacher;
        }
        return response()->json(array('status' => 'success', 'students' => $courses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // get all the nerds
        $teachers = Teachers::all();
        $languages  = Languages::all();
        $branches = Branches::where('is_active',1)->get();
        // load the view and pass the nerds
        return View::make('courses.create')
          ->with('teachers', $teachers)
          ->with('languages',$languages)
          ->with('branches', $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'level' => 'required',
            'teacher' => 'required',
            'start_daily_time' => 'required',
            'end_daily_time' => 'required',
            'days' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $course = new Courses();

        $course->name = $request->input('name');
        $course->level = $request->input('level');
        $course->notes = $request->input('notes');
        $course->branch_id = $request->input('branch');
        $course->start_daily_time = $request->input('start_daily_time');
        $course->end_daily_time = $request->input('end_daily_time');
        $course->days               =   '';
        for($x=0; $x<count($request->input('days')); $x++)
        {
            if(count($request->input('days'))-1 == $x)
                $course->days       .=   $request->input('days')[$x];
            else
                $course->days       .=   $request->input('days')[$x] . ',';
        }
        $course->start_date = $request->input('start_date');
        $course->end_date = $request->input('end_date');
        $course->monthlyCost        =   $request->input('monthlyCost'); //Added by CodeMajic for new billing functionatilies
        $course->inscriptionCost    =   $request->input('inscriptionCost'); //Added by CodeMajic for new billing functionatilies
        $course->save();

        $teacher = Teachers::find($request->input('teacher'));
        $language = Languages::find($request->input('language'));
        $course->teacher()->associate($teacher);
        $course->language()->associate($language);
        $course->save();

        $this->saveLog($course->id, $course, 'new', 'courses');

        return Redirect::to('courses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $course = Courses::find($id);
        $teachers = Teachers::all();
        $languages  = Languages::all();

        // load the view and pass the nerds
        return View::make('courses.show')
            ->with('course', $course)
            ->with('teachers', $teachers)
            ->with('languages',$languages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the nerds
        $course = Courses::find($id);
        $teachers = Teachers::all();
        $languages  = Languages::all();
        $branches = Branches::where('is_active',1)->get();
        // load the view and pass the nerds
        return View::make('courses.edit')
            ->with('course', $course)
            ->with('teachers', $teachers)
            ->with('languages',$languages)
            ->with('branches', $branches);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$this->validate($request, [
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'attendant' => 'required',
            'service' => 'required',
        ]);*/

        $course = Courses::find($id);
        $this->saveLog($id, $course, 'updateBefore', 'courses');

        $course->name = $request->input('name');
        $course->level = $request->input('level');
        $course->branch_id = $request->input('branch');
        $course->notes = $request->input('notes');
        $course->start_daily_time = $request->input('start_daily_time');
        $course->end_daily_time = $request->input('end_daily_time');
        $course->days               =   '';
        for($x=0; $x<count($request->input('days')); $x++)
        {
            if(count($request->input('days'))-1 == $x)
                $course->days       .=   $request->input('days')[$x];
            else
                $course->days       .=   $request->input('days')[$x] . ',';
        }
        $course->start_date = $request->input('start_date');
        $course->end_date = $request->input('end_date');
        $course->monthlyCost        =   empty($request->input('monthlyCost')) ? 0 : $request->input('monthlyCost'); //Added by CodeMajic for new billing functionatilies
        $course->inscriptionCost    =   empty($request->input('inscriptionCost')) ? 0 : $request->input('inscriptionCost'); //Added by CodeMajic for new billing functionatilies
        $course->save();

        $teacher = Teachers::find($request->input('teacher'));
        $language = Languages::find($request->input('language'));
        $course->teacher()->associate($teacher);
        $course->language()->associate($language);
        $course->save();

        $this->saveLog($id, $course, 'updateAfter', 'courses');

        return Redirect::to('courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Course = Courses::findOrFail($id);
        $this->saveLog($id, $Course, 'delete', 'courses');
        $Course->is_active = 0;
        $Course->save();
        //$Course->delete();
        return "success";
    }

}
