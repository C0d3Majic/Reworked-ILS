<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\AlertsModel;
use App\Models\Courses;
use App\Models\StudentAttendances;
use App\Models\TeacherAttendances;
use App\Models\StudentBilling;
use App\Models\Grades;
use App\Models\Students;
use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Redirect;
use Fpdf; //Facade for fpdf library

class GradesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the nerds
        $grades     =   Grades::orderBy('id','desc')->get();
        $courses    =   Courses::all();
        return View::make('grades.index')
            ->with('grades', $grades)->with('courses',$courses);
    }
    
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses    =   Courses::all();
        $students   =   Students::all();
        return View::make('grades.create')->with('courses',$courses)->with('students',$students);
    }

    /* Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       /*$this->validate($request, [
           'name' => 'required',
           'phone' => 'required|numeric',
           'email' => 'required|email',
           'attendant' => 'required',
           'service' => 'required',
       ]);*/
        $course = Courses::find($request->input('course'));
        $date1 = $course->start_date;
        $date2 = $course->end_date;

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        
        $student_already_has_months_captured = Grades::where('student_id',$request->input('student'))->where('course_id',$request->input('course'))->count();
        if($student_already_has_months_captured < $diff)
        {
            $grade                     =   new Grades();
            $grade->student_id         =   $request->input('student');
            $grade->course_id          =   $request->input('course');
            $grade->grade              =   $request->input('grade');
            $grade->month_number       =   $request->input('month');
            $grade->save();
        }


       return Redirect::to('grades');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       // get all the nerds

       // load the view and pass the nerds
       return View::make('billing.show')
           ->with('billing', '{}');
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       // load the view and pass the nerds
       $courses =   Courses::all();
       $grade   =   Grades::find($id);

        $date1 = $grade->course->start_date;
        $date2 = $grade->course->end_date;

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        
        return View::make('grades.edit')->with('grade',$grade)->with('months_in_course',$diff)->with('courses',$courses);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       /*$this->validate($request, [
           'name' => 'required',
           'phone' => 'required|numeric',
           'email' => 'required|email',
           'attendant' => 'required',
           'service' => 'required',
       ]);*/

        $grade                          =   Grades::find($id);
        $grade->course_id               =   $request->input('course');
        $grade->grade                   =   $request->input('grade');
        $grade->month_number       =   $request->input('month');
        $grade->save();
        return Redirect::to('grades');
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       $grade = Grades::findOrFail($id);
       $grade->delete();

       return "success";
    }
}