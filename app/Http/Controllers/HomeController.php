<?php

namespace App\Http\Controllers;

use App\Models\Departaments;
use App\Models\Roles;
use App\User;

use App\Http\Controllers\Controller as BaseController;
use App\Models\AlertsModel;
use Illuminate\Http\Request;

use App\Models\Machines;
use App\Models\Lines;
use App\Models\Shifts;
use App\Models\Codes;
use App\Models\EmailQueue;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

use View;
use \App\Traits;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Redirect::to('students');
    }
}
