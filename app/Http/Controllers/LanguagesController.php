<?php

namespace App\Http\Controllers;

use App\Models\Languages;
use App\Models\Branches;
use App\User;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use View;

class LanguagesController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 1){
            $branches = Branches::where('is_active',1)->get();
            $languages = Languages::orderBy('id','desc')->where('branch_id',1)->where('is_active',1)->get();
            return view('languages.index')
                ->with('languages', $languages)->with('branches',$branches);
        }else
        {
            $languages = Languages::orderBy('id','desc')->where('branch_id',Auth::user()->branch_id)->where('is_active',1)->get();
        return view('languages.index')
            ->with('languages', $languages);
    }
    }

    public function retrieve_language(Request $request)
    {
        $language_id = $request->input('language_id');
        $language = Languages::find($language_id);
        $modal_data = '<h5>Deleting language: <b>'. $language->name .'</b></h5>' ;
        return response()->json(array('status' => 'success', 'modal' => $modal_data));
    }

    public function change_branch(Request $request){
        $branch_id = $request->input('branch_id');
        $students = Languages::orderBy('id', 'desc')->where('branch_id',$branch_id)->where('is_active',1)->get();
        return response()->json(array('status' => 'success', 'students' => $students));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // get all the nerds
        //$Languages = Languages::all();
        $branches = Branches::where('is_active',1)->get();
        // load the view and pass the nerds
        return View::make('languages.create')->with('branches',$branches);
        //  ->with('Languages', $Languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request, [
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'attendant' => 'required',
            'service' => 'required',
        ]);*/

        $language = new Languages();

        $language->name = $request->input('name');
        $language->notes = $request->input('notes');
        $language->branch_id = $request->input('branch');
        $language->save();

        $this->saveLog($language->id, $language, 'new', 'languages');

        return Redirect::to('languages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $language = Languages::find($id);

        // load the view and pass the nerds
        return View::make('languages.show')
            ->with('language', $language);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the nerds
        $language = Languages::find($id);
        $branches = Branches::where('is_active',1)->get();
        // load the view and pass the nerds
        return View::make('languages.edit')
            ->with('language', $language)
            ->with('branches',$branches);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$this->validate($request, [
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'attendant' => 'required',
            'service' => 'required',
        ]);*/

        $language = Languages::find($id);
        $this->saveLog($id, $language, 'updateBefore', 'languages');

        $language->name = $request->input('name');
        $language->notes = $request->input('notes');
        $language->branch_id = $request->input('branch');
        $language->save();
        $this->saveLog($id, $language, 'updateAfter', 'languages');

        return Redirect::to('languages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Language = Languages::findOrFail($id);
        $this->saveLog($id, $Language, 'delete', 'Language');
        $Language->is_active = 0;
        $Language->save();
        return "success";
    }

}
