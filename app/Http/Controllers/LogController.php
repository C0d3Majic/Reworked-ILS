<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\User;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use View;

class LogController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = Log::all();

        return view('log.index')
            ->with('log', $log);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $log = Log::find($id);

        // load the view and pass the nerds
        return View::make('log.show')
            ->with('log', $log);

    }


}
