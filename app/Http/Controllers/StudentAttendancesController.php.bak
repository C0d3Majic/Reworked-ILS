<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\StudentAttendances;
use App\Models\Students;
use App\User;

//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller as BaseController;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Expr\Array_;
use View;

class StudentAttendancesController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentAttendances = StudentAttendances::orderBy('id','desc')->get();
        $courses            =   Courses::all();
        foreach($studentAttendances as $studentAttendance)
        {
            $studentName = Students::where('controlNumber',$studentAttendance->controlNumber)->first();
            $studentAttendance->name = $studentName->first_name . ' ' . $studentName->last_name;
            $studentAttendance->course  = Courses::find($studentAttendance->course_id)->name;
        }
        return view('studentAttendances.index')
            ->with('studentAttendances', $studentAttendances)->with('courses',$courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // get all the nerds
        $students = Students::all();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());
        $courses = Courses::all();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());

        // load the view and pass the nerds
        return View::make('studentAttendances.create')
            ->with('students', $students)
            ->with('courses', $courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'attendance_time' => 'required',
        ]);

        $studentAttendance = new StudentAttendances();

        $studentAttendance->controlNumber = Students::find($request->input('selectedStudentsIds'))->controlNumber;
        $studentAttendance->course_id = $request->input('selectedCoursesIds');
        $studentAttendance->attendance_datetime	 =$request->input('attendance_date').' '.$request->input('attendance_time');
        $studentAttendance->save();

        $this->saveLog($studentAttendance->id, $studentAttendance, 'new', 'studentAttendances');

        return Redirect::to('studentAttendances');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $StudentAttendance = StudentAttendances::find($id);

        // load the view and pass the nerds
        return View::make('studentAttendances.show')
            ->with('studentAttendance', $StudentAttendance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the nerds
        $studentAttendance  = StudentAttendances::where('controlNumber', $id)->first();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());
        $studentName = Students::where('controlNumber',$studentAttendance->controlNumber)->first();
        $studentAttendance->name = $studentName->first_name . " " . $studentName->last_name;
        $studentAttendance->course  = Courses::find($studentAttendance->course_id)->name;

        // load the view and pass the nerds
        return View::make('studentAttendances.edit')
            ->with('studentAttendance', $studentAttendance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'attendance_time' => 'required',
        ]);

        $studentAttendance = StudentAttendances::find($id);
        $this->saveLog($id, $studentAttendance, 'updateBefore', 'studentAttendances');

        $studentAttendance->attendance_datetime	 =$request->input('attendance_date').' '.$request->input('attendance_time');
        $studentAttendance->save();
        $this->saveLog($id, $studentAttendance, 'updateAfter', 'studentAttendances');

        return Redirect::to('studentAttendances');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $StudentAttendance = StudentAttendancesModel::findOrFail($id);
        $this->saveLog($id, $StudentAttendance, 'delete', 'studentAttendances');
        $StudentAttendance->delete();

        return "success";
    }

    public function retrieve_StudentAttendances(Request $request)
    {
        $status = $request->input('status');
        $between_date = $request->input('date');
        $between_date = explode(" - ",$between_date);
        $StudentAttendances = StudentAttendancesModel::where('status',$status)->whereBetween('StudentAttendance_datetime', [$between_date[0], $between_date[1]])->get();
        foreach($StudentAttendances as $StudentAttendance)
        {
            $StudentAttendance->machine = Machines::find($StudentAttendance->machine)->name;
            $StudentAttendance->line = Lines::find($StudentAttendance->line)->name;
            $StudentAttendance->shift = Shifts::find($StudentAttendance->shift)->name;
            $StudentAttendance->status = Codes::find($StudentAttendance->status)->description;
        }
        $status = Codes::all();
        return response()->json(array('status' => 'success', 'StudentAttendances' => $StudentAttendances, 'status' => $status));
        //return 'success';
    }

    public function record_StudentAttendance(Request $request)
    {
        $studentNumber = $request->input('studentNumber');
        $studentCourse = $request->input('studentCourse');

        if(empty($studentNumber))
            return response()->json(array('status' => 'error', 'type' => 'No haz ingresado tu matricula de estudiante'));
        if(empty($studentCourse))
            return response()->json(array('status' => 'error', 'type' => 'No haz ingresado el curso al que estas asistiendo'));
        
        $student = Students::where('controlNumber', $studentNumber)->first();
        if(!$student)
            return response()->json(array('status' => 'error', 'type' => 'Esta matricula no pertenece a ningun alumno'));
        
        $course = Courses::find($studentCourse);
        if(!$course) 
            return response()->json(array('status' => 'error', 'type' => 'Este curso no existe'));
        
        $studentAttendance = new StudentAttendances();
        $studentAttendance->controlNumber           =   $student->controlNumber;
        $studentAttendance->course_id               =   $course->id;
        $studentAttendance->attendance_datetime	    =   Carbon::now('America/Chihuahua');
        $studentAttendance->save();

        return response()->json(array('status' => 'success'));
    }

}
