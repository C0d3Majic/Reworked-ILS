<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\TeacherAttendances;
use App\Models\Teachers;
use App\User;

//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller as BaseController;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Expr\Array_;
use View;

class TeacherAttendancesController extends BaseController
{
    use \App\Traits\LogManager;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacherAttendances = TeacherAttendances::orderBy('id','desc')->get();
        $courses = Courses::all();
        foreach($teacherAttendances as $teacherAttendance)
        {
            $teacherName = Teachers ::where('controlNumber',$teacherAttendance->controlNumber)->first();
            $teacherAttendance->name = $teacherName->first_name . ' ' . $teacherName->last_name;
            $teacherAttendance->course  = Courses::find($teacherAttendance->course_id)->name;
        }
        return view('teacherAttendances.index')
            ->with('teacherAttendances', $teacherAttendances)->with('courses', $courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // get all the nerds
        $teachers = Teachers::all();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());
        $courses = Courses::all();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());

        // load the view and pass the nerds
        return View::make('teacherAttendances.create')
            ->with('teachers', $teachers)
            ->with('courses', $courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'attendance_time' => 'required',
            'type' => 'required',
        ]);

        $teacherAttendance = new TeacherAttendances();

        $teacherAttendance->controlNumber = Teachers::find($request->input('selectedTeachersIds'))->controlNumber;
        $teacherAttendance->course_id = $request->input('selectedCoursesIds');
        $teacherAttendance->attendance_datetime	 =$request->input('attendance_date').' '.$request->input('attendance_time');
        $teacherAttendance->type = $request->input('type');
        $teacherAttendance->save();

        $this->saveLog($teacherAttendance->id, $teacherAttendance, 'new', 'teacherAttendances');

        return Redirect::to('teacherAttendances');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get all the nerds
        $TeacherAttendance = TeacherAttendances::find($id);

        // load the view and pass the nerds
        return View::make('teacherAttendances.show')
            ->with('TeacherAttendance', $TeacherAttendance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the nerds
        $teacherAttendance  = TeacherAttendances::where('id', $id)->first();///Courses::whereDate('end_date', '>', Carbon::today()->toDateString());
        $teacher = Teachers::where('controlNumber',$teacherAttendance->controlNumber)->first();
        $teacherAttendance->name = $teacher->first_name . " " . $teacher->last_name;
        $teacherAttendance->course  = Courses::find($teacherAttendance->course_id)->name;
        // load the view and pass the nerds
        return View::make('teacherAttendances.edit')
            ->with('teacherAttendance', $teacherAttendance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'attendance_time' => 'required',
        ]);

        $teacherAttendance = TeacherAttendances::find($id);
        $this->saveLog($id, $teacherAttendance, 'updateBefore', 'teacherAttendances');

        $teacherAttendance->attendance_datetime	 =$request->input('attendance_date').' '.$request->input('attendance_time');
        $teacherAttendance->save();
        $this->saveLog($id, $teacherAttendance, 'updateAfter', 'teacherAttendances');

        return Redirect::to('teacherAttendances');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $TeacherAttendance = TeacherAttendancesModel::findOrFail($id);
        $this->saveLog($id, $TeacherAttendance, 'delete', 'teacherAttendances');
        $TeacherAttendance->delete();

        return "success";
    }

    public function retrieve_TeacherAttendances(Request $request)
    {
        $status = $request->input('status');
        $between_date = $request->input('date');
        $between_date = explode(" - ",$between_date);
        $TeacherAttendances = TeacherAttendancesModel::where('status',$status)->whereBetween('TeacherAttendance_datetime', [$between_date[0], $between_date[1]])->get();
        foreach($TeacherAttendances as $TeacherAttendance)
        {
            $TeacherAttendance->machine = Machines::find($TeacherAttendance->machine)->name;
            $TeacherAttendance->line = Lines::find($TeacherAttendance->line)->name;
            $TeacherAttendance->shift = Shifts::find($TeacherAttendance->shift)->name;
            $TeacherAttendance->status = Codes::find($TeacherAttendance->status)->description;
        }
        $status = Codes::all();
        return response()->json(array('status' => 'success', 'TeacherAttendances' => $TeacherAttendances, 'status' => $status));
        //return 'success';
    }

    public function record_TeacherAttendance(Request $request)
    {
        $teacherNumber = $request->input('teacherNumber');
        $teacherCourse = $request->input('teacherCourse');
        $entryType = $request->input('entryType');

        if(empty($teacherNumber))
            return response()->json(array('status' => 'error', 'type' => 'No haz ingresado tu codigo de maestro'));
        if(empty($teacherCourse))
            return response()->json(array('status' => 'error', 'type' => 'No haz ingresado el curso al que estas asistiendo'));
        if(empty($entryType))
            return response()->json(array('status' => 'error', 'type' => 'No haz ingresado si es una entrada o salida'));
        $teacher = Teachers::where('controlNumber', $teacherNumber)->first();
        if(!$teacher)
            return response()->json(array('status' => 'error', 'type' => 'Ese codigo no pertenece a ningun maestro'));
        
        $course = Courses::find($teacherCourse);
        if(!$course) 
            return response()->json(array('status' => 'error', 'type' => 'Este curso no existe'));
        
        $last_entry = TeacherAttendances::where('controlNumber',$teacherNumber)->orderBy('id','desc')->first();
        if($last_entry)
        {
            if(($last_entry->type == "Entrada" && $entryType == "Entrada") || ($last_entry->type == "Salida" && $entryType == "Salida"))
                return response()->json(array('status' => 'error', 'type' => 'No es posible almacenar otra entrada o salida consecutiva para este maestro'));
        }else
        {
            if($entryType == "Salida")
                return response()->json(array('status' => 'error', 'type' => 'No es posible almacenar una salida sin una entrada'));
        }
        
            $teacherAttendance = new TeacherAttendances();
        $teacherAttendance->controlNumber           =   $teacher->controlNumber;
        $teacherAttendance->course_id               =   $course->id;
        $teacherAttendance->type                    =   $entryType;
        $teacherAttendance->attendance_datetime	    =   Carbon::now('America/Chihuahua');
        $teacherAttendance->save();

        return response()->json(array('status' => 'success'));
    }

}
