<?php

namespace App\Http\Controllers;

use App\Models\Departaments;
use App\Models\Escalation_Levels;
use App\Models\Roles;
use Illuminate\Routing\Controller as BaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use View;
use Illuminate\Support\Facades\Hash;

class UsersController extends BaseController
{
    use \App\Traits\LogManager;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the nerds
        $users = User::orderBy('id','desc')->get();

        /*foreach($users as $user)
        {
            $user->role = Roles::find($user->role)->name;
        }*/

        return View::make('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the view and pass the nerds
        return Redirect::to('register');
    }

    public function show($id)
    {
        $user = User::find($id);
        $roles = Roles::pluck('name', 'id');

        // load the view and pass the nerds
        return View::make('users.show')
            ->with('user', $user)
            ->with('roles',$roles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
//        $roles = Roles::pluck('name', 'id');

        // load the view and pass the nerds
        return View::make('users.edit')
            ->with('user', $user);
//            ->with('roles',$roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // get all the nerds
        $user = User::find($id);
        $this->saveLog($id, $user, 'updateBefore', 'users');

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->name_tag = $request->input('name_tag');
        $user->role = $request->input('role');

        if($request->input('new_password') != '')
            $user->password = Hash::make($request->input('new_password'));

        $user->save();
        $this->saveLog($id, $user, 'updateAfter', 'users');

        // redirect
        return Redirect::to('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->saveLog($id, $user, 'delete', 'users');
        $user->delete();

        return "success";
    }
}
