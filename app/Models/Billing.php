<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billing';
    public function course()
    {
        return $this->belongsTo('App\Models\Courses');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Students');
    }

}
