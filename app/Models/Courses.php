<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teachers');
    }

    public function students_in_course()
    {
        return $this->belongsToMany('App\Models\Students', 'student_courses', 'course_id', 'student_id');
    }

    public function language()
    {
        return $this->belongsTo('App\Models\Languages');
    }
}
