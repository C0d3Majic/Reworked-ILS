<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $table = 'grades';
    public function course()
    {
        return $this->belongsTo('App\Models\Courses');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Students');
    }

}
