<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAttendances extends Model
{
    protected $table = 'student_attendances';
}
