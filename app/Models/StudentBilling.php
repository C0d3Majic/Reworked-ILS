<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentBilling extends Model
{
    protected $table = 'student_billing';

    public function student()
    {
        return $this->belongsTo('App\Models\Students');
    }
}
