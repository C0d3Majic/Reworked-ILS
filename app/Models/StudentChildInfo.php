<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentChildInfo extends Model
{
    protected $table = 'student_child_info';
}
