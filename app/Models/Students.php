<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{

    public function courses()
    {
        return $this->belongsToMany('App\Models\Courses', 'student_courses', 'student_id', 'course_id');
    }

}
