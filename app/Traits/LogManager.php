<?php namespace App\Traits;

use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait LogManager {

    public function saveLog($contentId, $content, $type, $table) {

        $logRecord = new App\Models\Log();

        $logRecord ->date_time=Carbon::now('America/Chihuahua');
        $logRecord ->user_id=Auth::User()->id;
        $logRecord ->content_id=$contentId;
        if($content!=null) $content = substr($content,0,1023);
        $logRecord ->content=$content;
        $logRecord ->type=$type;
        $logRecord ->table_name=$table;

        $logRecord->save();
    }

}