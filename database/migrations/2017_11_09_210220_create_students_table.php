<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {

            $table->increments('id');

            $table->dateTime('registration_date');
            $table->string('first_name');
            $table->string('last_name');
            $table->dateTime('birthdate');
            $table->integer('age');
            $table->string('ocupation');
            $table->string('address');
            $table->string('phone');
            $table->string('company');
            $table->string('institution');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
