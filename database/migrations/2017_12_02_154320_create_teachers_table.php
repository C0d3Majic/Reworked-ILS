<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {

            $table->increments('id');

            $table->dateTime('registration_date');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address');
            $table->string('telephone1');
            $table->string('telephone2');
            $table->string('RFC');
            $table->decimal('salary_per_hour');
            $table->string('known_languages');
            $table->time('current_checkin');
            $table->time('current_checkout');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
