<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'El nombre o contraseña no se encuentra en la base de datos.',
    'throttle' => 'Se alcanzo la cantidad máxima de intentos, vuelva a intentar en :seconds segundos.',
//    Too many login attempts. Please try again in :seconds seconds.',

];
