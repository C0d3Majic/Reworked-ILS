<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

//    'password' => 'Passwords must be at least six characters and match the confirmation.',
//    'reset' => 'Your password has been reset!',
//    'sent' => 'We have e-mailed your password reset link!',
//    'token' => 'This password reset token is invalid.',
//    'user' => "We can't find a user with that e-mail address.",

    'password' => 'La contraseña debe contenter al menos 6 characters y corresponder en la confirmación.',
    'reset' => 'La contraseña ha sido reiniciada!',
    'sent' => 'Enviamos un correo al usuario con el enlace para cambiar la contraseña',
    'token' => 'La llave para reiniciar la contraseña es incorrecta.',
    'user' => "No es posible encontrar un usuario con el correo electrónico especificado.",
];
