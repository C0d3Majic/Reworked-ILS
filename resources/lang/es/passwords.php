<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

//    'password' => 'Las contraseñas deben coincidir y contener al menos 6 caracteres',
    'password' => 'La contraseña debe tener un mínimo de diez (10) caracteres y pertenecer al menos 
     a tres de las siguientes categorias:\n
         Al menos un carácter alfanumérico en mayúsculas\n
         Al menos un carácter alfanumérico en minúsculas\n
         Al menos un número [0-9]\n
         Al menos un carácter especial [&, #,!, Etc.]',
    'reset'    => '¡Tu contraseña ha sido restablecida!',
    'sent'     => '¡Te hemos enviado por correo el enlace para restablecer tu contraseña!',
    'token'    => 'El token de recuperación de contraseña es inválido.',
    'user'     => 'No podemos encontrar ningún usuario con ese correo electrónico.',

];
