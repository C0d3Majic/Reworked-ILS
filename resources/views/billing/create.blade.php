@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Agregar un nuevo pago</span>
                            <div class="row">
                                <form class="col s12" method="POST" class="form-horizontal" role="form" action="{{ action('BillingController@store') }}">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Alumno</label>
                                        <select name="student" class="browser-default">
                                            <option value="" disabled selected>Seleccione el alumno</option>
                                            <?php 
                                                foreach($students as $student)
                                                {
                                                    echo '<option value='.$student->id.'>'.$student->first_name.'</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Curso que pago</label>
                                        <select name="course" class="browser-default">
                                            <option value="" disabled selected>Seleccione el curso</option>
                                            <?php 
                                                foreach($courses as $course)
                                                {
                                                    echo '<option value='.$course->id.'>'.$course->name.'</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Tipo de Pago</label>
                                        <select name="payment_type" class="browser-default">
                                            <option style="font-size:15px" value="" disabled selected>Seleccione el tipo de pago</option>
                                            <option style="font-size:15px" value="0">Inscripción</option>
                                            <option style="font-size:15px" value="1">Primer Pago</option> 
                                            <option style="font-size:15px" value="2">Segundo Pago</option>
                                            <option style="font-size:15px" value="3">Tercer Pago</option>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                        <input name="amount_paid" id="icon_prefix" type="number">
                                            <label for="student">Cantidad</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s2">
                                            <button class="btn-block btn-large waves-effect waves-light green"
                                                    type="submit">Guardar
                                                <i class="material-icons right">check</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/billing/create");
        $("#listmenu").attr("href", "/billing");
    </script>
@endsection
