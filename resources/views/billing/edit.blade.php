@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Editar pago</span>
                            <div class="row">
                                <div class="row">
                                    {!! Form::model($payment, ['method' => 'PATCH','route' => ['billing.update', $payment->id],'class'=> 'col s12', 'role'=>'form']) !!}
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                        <input name="student" id="icon_prefix" type="text"
                                            value="{{ $payment->student->first_name }}" readonly>
                                            <label for="student">Alumno</label>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Curso que pago</label>
                                        <select name="course" class="browser-default">
                                            <option value="" disabled>Seleccione el curso</option>
                                            <?php 
                                                foreach($courses as $course)
                                                {
                                                    if($course->id == $payment->course->id)
                                                        echo '<option value='.$course->id.' selected>'.$course->name.'</option>';
                                                    else
                                                    {
                                                        echo '<option value='.$course->id.'>'.$course->name.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Tipo de Pago</label>
                                        <select name="payment_type" class="browser-default">
                                        {{$payment->course->number_of_payment}}
                                            <option style="font-size:15px" value="" disabled>Seleccione el tipo de pago</option>
                                            @if($payment->number_of_payment == 0 )<option style="font-size:15px" value="0" selected>Inscripción</option>@else <option style="font-size:15px" value="0">Inscripción</option> @endif
                                            @if($payment->number_of_payment == 1 )<option style="font-size:15px" value="1" selected>Primer Pago</option>@else <option style="font-size:15px" value="1">Primer Pago</option> @endif
                                            @if($payment->number_of_payment == 2 )<option style="font-size:15px" value="2" selected>Segundo Pago</option>@else <option style="font-size:15px" value="2">Segundo Pago</option> @endif
                                            @if($payment->number_of_payment == 3 )<option style="font-size:15px" value="3" selected>Tercer Pago</option>@else <option style="font-size:15px" value="3">Tercer Pago</option> @endif
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                        <input name="amount_paid" id="icon_prefix" type="number"
                                            value="{{ $payment->amount_paid }}">
                                            <label for="student">Cantidad</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::submit('Actualizar', array('class' => 'btn-block btn-large waves-effect waves-light green')) }}
                                        </div>
                                    </div>
                                    {{ Form::Close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/billing/create");
        $("#listmenu").attr("href", "/billing");
    </script>
@endsection
