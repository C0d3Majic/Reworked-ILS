@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
    </style>
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->
        <div class="col s10">

            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Listado de pagos</span>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="parts" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Curso</th>
                                            <th style="text-align:center">Numero de pago</th>
                                            <th style="text-align:center">Cantidad </th>
                                            <th style="text-align:center">Fecha del Pago</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($payments as $payment)
                                            <tr>
                                                <td>{{$payment->id}}</td>
                                                <td>{{$payment->student->first_name}}</td>
                                                <td>{{$payment->course->name}}</td>
                                                <td style="text-align:center">{{$payment->type_of_payment}}</td>
                                                <td style="text-align:center">{{$payment->amount_paid}}</td>
                                                <td style="text-align:center">{{$payment->created_at}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <h5>Reporte de pagos por curso</h5>
                                <div class="col s6">  
                                    <label class="active">Cursos Activos</label>
                                    <select id="courses">
                                        <option value="" disabled selected>Escoje el curso</option>
                                        @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                    </select> 
                                </div>
                                <div class="input-field col s6"> 
                                <a id="reporte" class="waves-effect waves-light btn-large"><i class="material-icons right">assignment</i>Generar Reporte</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /*Ajax funtions for welcome page*/
        $(document).ready(function () {
            $('#reporte').click(function(){
                var course_id;
                course_id = $("#courses").val();
                if(course_id == null || course_id == "")
                    Materialize.toast('<span>Falta escojer un curso</span>', 2000, 'rounded red darken-3');
                else
                    window.open("{{url('billing/payment_report')}}/"+course_id,"_blank");
            });
            $selectedId = 0;
            var parts = $('#parts').DataTable({
                select: true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                    "select": {
                        rows: " / %d selecionado(s)"
                    },
                },
                "order": [[0, "desc"]],
                responsive: true,
                "columnDefs": [
                    {"width": "10%", "targets": 0}
                ]
            });

            parts
                .on('select', function (e, dt, type, indexes) {
                    var rowData = parts.rows(indexes).data().toArray();
                    $selectedId = rowData[0][0]
                    $("#editmenu").attr("href", "/billing/" + $selectedId + "/edit");
                    $("#showmenu").attr("href", "/billing/" + $selectedId );
                })

            var table = $('#alerts').DataTable();
            $("#deletemenu").click(function () {
                bootbox.confirm({
                    message: "Estas seguro que deseas eliminar este registro de la base de datos?",
                    backdrop: false,
                    size: "small",
                    buttons: {
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-danger'
                        },
                        confirm: {
                            label: 'Eliminar',
                            className: 'btn-success'
                        },
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                url: '/billing/' + $selectedId,
                                type: 'DELETE',
                                data: {'_token': '{!! csrf_token() !!}'},
                                async: false,
                                success: function (result) {
                                    console.log(result);
                                    if (result == "success") {
                                        setTimeout(function () {// wait for 1 secs(2)
                                            location.reload();
                                        }, 1000);
                                    }
                                }
                            });
                        }
                    }
                });
            });

        });
    </script>
    <script>
        $("#createmenu").attr("href", "/billing/create");
        $("#listmenu").attr("href", "/billing");
    </script>
@endsection
