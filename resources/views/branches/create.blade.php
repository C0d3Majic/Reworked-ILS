@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Agregar una nueva sucursal</span>
                            <div class="row">
                                <form class="col s12" method="POST" class="form-horizontal" role="form" action="{{ action('BranchesController@store') }}">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s3">
                                        <input name="name" id="icon_prefix" type="text"
                                            value="" >
                                            <label for="name">Nombre de la sucursal</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="address" id="icon_prefix" type="text"
                                            value="" >
                                            <label for="address">Direccion</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="phone" id="icon_prefix" type="text"
                                            value="" >
                                            <label for="phone">Telefono</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="contact" id="icon_prefix" type="text"
                                            value="" >
                                            <label for="contact">Contacto</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s2">
                                            <button class="btn-block btn-large waves-effect waves-light green"
                                                    type="submit">Guardar
                                                <i class="material-icons right">check</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/branches/create");
        $("#listmenu").attr("href", "/branches");
    </script>
@endsection
