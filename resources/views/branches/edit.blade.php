@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Editar Sucursal</span>
                            <div class="row">
                                <div class="row">
                                    {!! Form::model($branch, ['method' => 'PATCH','route' => ['branches.update', $branch->id],'class'=> 'col s12', 'role'=>'form']) !!}
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s3">
                                        <input name="name" id="icon_prefix" type="text"
                                            value="{{ $branch->name }}" >
                                            <label for="name">Nombre de la sucursal</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="address" id="icon_prefix" type="text"
                                            value="{{ $branch->address }}" >
                                            <label for="address">Direccion</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="phone" id="icon_prefix" type="text"
                                            value="{{ $branch->phone }}" >
                                            <label for="phone">Telefono</label>
                                        </div>
                                        <div class="input-field col s3">
                                        <input name="contact" id="icon_prefix" type="text"
                                            value="{{ $branch->contact }}" >
                                            <label for="contact">Contacto</label>
                                        </div>
                                    </div>
                                    <br>   
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::submit('Actualizar', array('class' => 'btn-block btn-large waves-effect waves-light green')) }}
                                        </div>
                                    </div>
                                    {{ Form::Close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/billing/create");
        $("#listmenu").attr("href", "/billing");
    </script>
@endsection
