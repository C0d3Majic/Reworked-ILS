@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
        .modal{
            max-height:18%;
        }
    </style>
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->
        <div class="col s10">

            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Listado de Sucursales</span>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="parts" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Curso</th>
                                            <th style="text-align:center">Phone</th>
                                            <th style="text-align:center">Contacto </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($branches as $branch)
                                            <tr>
                                                <td>{{$branch->id}}</td>
                                                <td>{{$branch->name}}</td>
                                                <td>{{$branch->address}}</td>
                                                <td style="text-align:center">{{$branch->phone}}</td>
                                                <td style="text-align:center">{{$branch->contact}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal2" class="modal">
            <div class="modal-content">
                <h4>Eliminar curso</h4>
                <p>Estas seguro de querer eliminar este curso?</p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col s6">
                        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>
                    </div>
                    <div class="col s6">
                        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    <script>
        /*Ajax funtions for welcome page*/
        $(document).ready(function () {
            $('.modal').modal({dismissible: false} );
            $selectedId = 0;
            var parts = $('#parts').DataTable({
                select: true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                    "select": {
                        rows: " / %d selecionado(s)"
                    },
                },
                "order": [[0, "desc"]],
                responsive: true,
                "columnDefs": [
                    {"width": "10%", "targets": 0}
                ]
            });

            parts
                .on('select', function (e, dt, type, indexes) {
                    var rowData = parts.rows(indexes).data().toArray();
                    $selectedId = rowData[0][0]
                    $("#editmenu").attr("href", "/branches/" + $selectedId + "/edit");
                    $("#showmenu").attr("href", "/branches/" + $selectedId );
                })

            var table = $('#parts').DataTable();
            $('#deletemenu').click(function(){
                    $.ajax({
                    // The URL for the request. variable set above
                    url: "{{url('branch/retrieve')}}",
                    // The data to send (will be converted to a query string). variable set above
                    data: { branch_id: $selectedId },
                    // Whether this is a POST or GET request
                    type: "POST",
                    // The type of data we expect back. can be json, html, text, etc...
                    dataType : "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // Code to run if the request succeeds;
                    // the response is passed to the function
                    success: function( msg ) {
                        if(msg.status == 'error'){
                            var $toastContent = $('<span>'+ msg.type +'</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }else if(msg.status == 'success'){
                            $('.modal-content').html(msg.modal);
                            $('#modal2').modal('open',{dismissible:false});
                        }
                    },
                    error: function(){
                        //window.location.reload();
                        var $toastContent = $('<span>Oops... There is an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                    });
                });

                $('#btnEliminar').click(function(){
                    $.ajax({
                        url: '/branches/' + $selectedId,
                        type: 'DELETE',
                        data: {'_token': '{!! csrf_token() !!}'},
                        async: false,
                        success: function (result) {
                            console.log(result);
                            if (result == "success") {
                                Materialize.toast('<span>Branch deleted successfully</span>', 2000, 'rounded green darken-3');
                                setTimeout(function () {// wait for 5 secs(2)
                                    location.reload();
                                }, 1000);
                            }else
                            {
                                var $toastContent = $('<span>There was an error deleting the course</span>');
                                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                            }
                        }
                    });
                });

        });
    </script>
    <script>
        $("#createmenu").attr("href", "/branches/create");
        $("#listmenu").attr("href", "/branches");
    </script>
@endsection
