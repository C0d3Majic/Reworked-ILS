@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->
        <div class="col s10">

            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Ver usuario</span>
                        <div class="row">
                            <div class="row">
                                {!! Form::model($user, ['method' => 'PATCH','route' => ['users.show', $user->id],'class'=> 'col s12', 'role'=>'form']) !!}
                                <br>
                                <div class="row">
                                    <div class="input-field col s4">
                                        {{ Form::text('name', $user->name, array('style' => 'font-size:15px;', 'readonly' => 'true' )) }}
                                        {{ Form::label('name', 'nombre', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="input-field col s4">
                                        {{ Form::text('email', $user->email, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                        {{ Form::label('email', 'correo electrónico', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="input-field col s4">
                                        {{ Form::text('name_tag', $user->name_tag, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                        {{ Form::label('name_tag', 'gaffete', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="input-field col s4">
                                        {{ Form::text('name_tag', \App\Models\Roles::find($user->role)->name, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                        {!! Form::Label('role', 'Selecciona el rol del usuario', array('style' => 'font-size:15px;', 'class' => 'active')) !!}
                                    </div>
                                </div>
                                <br>
                                {{ Form::Close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
