@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
        .fc-today {
            background: #0C1959 !important;
            color:white;
            font-size:22px;
        } 
    </style>
    <div class="container">
        <!-- Left side of                                                                                                                                                                                                                                                                                                                                       the view AKA THE CRUD MENU -->
    @include("partials.crudmenu3")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s4">
                                <h5> Calendario</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                <div id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Structure -->
        <div id="modal1" class="modal" style="max-height: 37% !important;">
          <div class="modal-content">
          </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.modal').modal({dismissible:false});
                var courses = {!! json_encode($courses) !!};
                console.log(courses)
                var getEvent = []; 
                
                for(var i = 0;i < courses.length;i++)
                {                    
                    var course = courses[i];

                    // array of events
                    //var getEvent = [];  <-- move it out of loop
                    // inserting data from database to getEvent array
                    var insertEvents = {};
                    insertEvents =
                    {
                        title: 'Inicio de ' + course.name,
                        start: course.start_date,
                        color: '#0C1959',
                        id: course.id
                    }
                    getEvent.push(insertEvents); //Insert the start date

                    insertEvents = {};
                    insertEvents =
                    {
                        title: 'Fin de ' + course.name,
                        start: course.end_date,
                        color: 'red',
                        id: course.id,
                        notes : course.notes,
                    }
                    getEvent.push(insertEvents);
                }

                $('.fc-button-prev span').click(function(){
                    
                })

                $('#calendar:not(".fc-event")').on('contextmenu', function (e) {
                    e.preventDefault()
                })
                $('#modal1:not(".fc-event")').on('contextmenu', function (e) {
                    e.preventDefault()
                })

                $('#calendar').fullCalendar({
                    eventClick: function(calEvent) {
                        Materialize.toast(calEvent.title,2000, 'rounded navy blue') // 'rounded' is the class I'm applying to the toast
                        location.href = "{{url('courses')}}/"+calEvent.id;
                    },
                    dayClick: function(date, jsEvent, view) {                                      
                        Materialize.toast('Hoy es ' + date.format(),2000, 'rounded navy blue')
                    },
                    eventMouseover: function( calEvent, event, jsEvent, view ) { 
                        Materialize.toast(calEvent.notes,2000, 'rounded orange')
                    },
                // put your options and callbacks here
                    locale: 'es',
                    handleWindowResize: true,
                    displayEventTime: true, // Display event time
                    events: getEvent,
                    eventRender: function (event, element) {
                        element.bind('mousedown', function (e) {
                            if (e.which == 3) {
                               //EDIT AJAX FOR DESCRIPTION
                                var course_id;
                                course_id = event.id;
                                console.log(course_id);

                                $.ajax({
                                  // The URL for the request. variable set above
                                  url: "{{url('course/edit')}}",
                                  // The data to send (will be converted to a query string). variable set above
                                  data: { course_id: course_id },
                                  // Whether this is a POST or GET request
                                  type: "POST",
                                  // The type of data we expect back. can be json, html, text, etc...
                                  dataType : "json",
                                  headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                                  // Code to run if the request succeeds;
                                  // the response is passed to the function
                                  success: function( msg ) {
                                    if(msg.status == 'error'){
                                      var $toastContent = $('<span>'+ msg.type +'</span>');
                                      Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                                    }else if(msg.status == 'success'){
                                      $('.modal-content').html(msg.modal);
                                      $('#modal1').modal('open',{dismissible:false});

                                      $('#btnGuardar').click(function(){
                                        // Start $.ajax() method
                                        var jsonObject = {
                                            notes : $("#notes").val(),
                                            course_id : course_id
                                        }
                                        console.log(jsonObject);
                                        $.ajax({
                                          // The URL for the request. variable set above
                                          url: "{{url('course/description/save_edit')}}",
                                          // The data to send (will be converted to a query string). variable set above
                                          data: jsonObject,
                                          headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                          },
                                          // Whether this is a POST or GET request
                                          type: "POST",
                                          // The type of data we expect back. can be json, html, text, etc...
                                          dataType : "json",
                                          // Code to run if the request succeeds;
                                          // the response is passed to the function
                                          success: function( msg ) {
                                            if(msg.status == 'error'){
                                              var $toastContent = $('<span>'+ msg.type +'</span>');
                                              Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                                            }else if(msg.status == 'success'){
                                              Materialize.toast('<span>Descripcion actualizada exitosamente</span>', 2000, 'rounded green darken-3');
                                              setTimeout(function () {
                                                window.location.reload();
                                              }, 2000);
                                            }
                                          },
                                          error: function(){
                                            //window.location.reload();
                                            var $toastContent = $('<span>Oops... There is an error on the server</span>');
                                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                                          }
                                        });
                                      });

                                    }
                                  },
                                  error: function(){
                                    //window.location.reload();
                                    var $toastContent = $('<span>Oops... There is an error on the server</span>');
                                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                                  }
                                });
                               //******

                            }
                        });
                    }
                });
                var table = $('#log').DataTable({
                    select: true,
                });

                table
                    .on('select', function (e, dt, type, indexes) {
                        var rowData = table.rows(indexes).data().toArray();
                        $selectedId = rowData[0][0]
                        $("#editmenu").attr("href", "/log/" + $selectedId + "/edit");
                        $("#showmenu").attr("href", "/log/" + $selectedId );
                    })

            });

        </script>

        <!--
          Content Section End
        -->
@endsection

