@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Actualizar curso</span>
                        <div class="row">
                            <form action="{{ route('CoursesController.update', $course->id) }}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <h5 style="text-align: center">Datos del curso</h5>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="name" id="icon_prefix" type="text" value={{ $course->name }}>
                                        <label for="name">Nombre del curso</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <select id="languages" name="language" >
                                            <option value="" disabled selected>Lenguaje</option>
                                            @foreach($languages as $language)
                                                <option value={{ $language->id }}
                                                        @if($language->id == $course->language_id)
                                                            selected="selected"
                                                        @endif
                                                >{{ $language->name }}</option>
                                            @endforeach
                                        </select>
                                        <label>Lenguaje</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input name="level" id="icon_prefix" type="text" value={{ $course->level }}>
                                        <label for="level">Nivel</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <select name="branch">
                                            <option value="" disabled>Sucursal</option>
                                            @foreach($branches as $branch)
                                                @if($branch->id == $course->branch_id)
                                                    <option value="{{$branch->id}}" selected>{{$branch->name}}</option>
                                                @else
                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <label>Sucursal</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s10">
                                        <input name="notes" id="icon_prefix" type="text" value={{ $course->notes }}>
                                        <label for="notes">Notas</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s4">

                                        <select multiple name="days[]">
                                            <option value="" disabled>Dias</option>
                                            <?php 
                                                $array_of_days  =   explode(',', $course->days);
                                                $y=0;
                                                for($x=1; $x<=7; $x++)
                                                {              
                                                    switch($x)
                                                    {
                                                        case 1  :    $day   =     'Lunes';        break;
                                                        case 2  :    $day   =     'Martes';       break;
                                                        case 3  :    $day   =     'Miercoles';    break;
                                                        case 4  :    $day   =     'Jueves';       break;
                                                        case 5  :    $day   =     'Viernes';      break;
                                                        case 6  :    $day   =     'Sabado';       break;
                                                        case 7  :    $day   =     'Domingo';      break;
                                                    }                             
                                                    if($y<count($array_of_days) && $array_of_days[$y] == "".$x."")
                                                    {
                                                        echo '<option value='.$x.' selected>'.$day.'</option>';
                                                        $y++;
                                                    }
                                                    else
                                                        echo '<option value='.$x.'>'.$day.'</option>';
                                                }
                                            ?>
       
                                        </select>
                                        <label style="font-size:15px;" class="active"
                                               for="icon_prefix">Dias que se imparte el curso</label>

                                        <!--input name="days" id="icon_prefix" type="text" value={{ $course->days }}>
                                        <label for="days">Días (1,2,3... donde lunes es 1) </label-->
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="start_daily_time" id="icon_prefix" type="text" value={{ $course->start_daily_time }}>
                                        <label for="start_daily_time">Inicia a la hora</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="end_daily_time" id="icon_prefix" type="text" value={{ $course->end_daily_time }}>
                                        <label for="end_daily_time">Termina a la hora</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s3">
                                        <input name="start_date" id="icon_prefix" type="text" class="timepicker"
                                               value={{ $language->start_date }}>
                                        <label for="start_date">Fecha de inicio de curso</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="end_date" id="icon_prefix" type="text" class="timepicker" value={{$course->end_date}}>
                                        <label for="end_date">Fecha de fin de curso</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <select id="teachers" name="teacher" class="browser-default">
                                            @foreach($teachers as $teacher)
                                                <option value={{ $teacher->id }}
                                                        @if($teacher->id == $course->teacher_id)
                                                            selected="selected"
                                                        @endif
                                                >{{ $teacher->last_name.' '.$teacher->first_name }}</option>
                                            @endforeach
                                        </select>
                                        <label style="font-size:15px;" class="active"
                                               for="icon_prefix">Profesor que impartira el curso?</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <input name="inscriptionCost" id="icon_prefix" type="text" value="{{ $course->inscriptionCost }}">
                                        <label for="inscriptionCost">Costo de inscripción sugerido</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="monthlyCost" id="icon_prefix" type="text" value="{{ $course->monthlyCost }}">
                                        <label for="start_daily_time">Costo mensual sugerido</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <button class="btn-block btn-large waves-effect waves-light green"
                                                type="submit">Actualizar
                                            <i class="material-icons right">check</i>
                                        </button>
                                    </div>
                                    <div class="col s10">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $('.timepicker').daterangepicker(
                {
                    singleDatePicker: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    },
                },
                function (start, end, label) {
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });
            
                var jsonObject = {
                language_id : $("#languages").val()
            }
            $('#teachers').empty();
            // Start $.ajax() method
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('retrieve/teachers')}}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }else if(msg.status == 'success')
                    {
                        for(i=0; i<msg.teachers.length; i++)
                            $('#teachers').append('<option value="'+msg.teachers[i].id+'">'+msg.teachers[i].first_name + " " + msg.teachers[i].last_name+'</option>');

                    }
                },
                error: function(){
                var $toastContent = $('<span>There was an error on the server</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }
            });
            return false;
        });
        $("#languages").on('change',function(){
            var jsonObject = {
                language_id : $("#languages").val()
            }
            $('#teachers').empty();
            // Start $.ajax() method
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('retrieve/teachers')}}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }else if(msg.status == 'success')
                    {
                        for(i=0; i<msg.teachers.length; i++)
                            $('#teachers').append('<option value="'+msg.teachers[i].id+'">'+msg.teachers[i].first_name + " " + msg.teachers[i].last_name+'</option>');

                    }
                },
                error: function(){
                var $toastContent = $('<span>There was an error on the server</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }
            });
            return false;
            

        })
        $("#createmenu").attr("href", "/courses/create");
        $("#listmenu").attr("href", "/courses");

    </script>
@endsection