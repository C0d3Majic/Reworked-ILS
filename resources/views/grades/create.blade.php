@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Agregar una nueva calificación</span>
                            <div class="row">
                                <form class="col s12" method="POST" class="form-horizontal" role="form" action="{{ action('GradesController@store') }}">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <br>
                                    <div class="row">
                                        <div class="col s4">
                                        <label class="active">Alumno</label>
                                        <select name="student" class="browser-default">
                                            <option value="" disabled selected>Seleccione el alumno</option>
                                            <?php 
                                                foreach($students as $student)
                                                {
                                                    echo '<option value='.$student->id.'>'.$student->first_name.'</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>
                                        <div class="col s4">
                                        <label>Curso a calificar</label>
                                            <select id="course" name="course" class="browser-default">
                                            <option value="" disabled selected>Seleccione el curso</option>
                                            <?php 
                                                foreach($courses as $course)
                                                {
                                                    echo '<option value='.$course->id.'>'.$course->name.'</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="input-field col s4">
                                        <input name="grade" id="grade" type="number">
                                            <label for="grade" style="font-size:15px">Calificación</label>
                                        </div>
                                        <div class="col s4">
                                            <label class="active" style="font-size:15px">Mes calificado</label>
                                            <select id="month" name="month" class="browser-default">
                                                <option value="" disabled>Seleccione el Mes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s2">
                                            <button class="btn-block btn-large waves-effect waves-light green"
                                                    type="submit">Guardar
                                                <i class="material-icons right">check</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $( document ).ready(function() {
        $("#course").on('change',function(){
            var course_id = $("#course").val();
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('course/lenght')}}/"+course_id,
                // The data to send (will be converted to a query string). variable set above
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "GET",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }else{
                        var numero_de_meses = msg.lenght;
                        numero_de_meses = numero_de_meses == 0 ? 1 : numero_de_meses; 
                        var x;
                        $('#month').children('option:not(:first)').remove();
                        for(x=1; x<=numero_de_meses; x++)
                        {
                            $("#month").append($("<option>").attr("value",x).text(x)); 
                        }
                        $('select').material_select();
                    }
                },
                error: function(){
                    var $toastContent = $('<span>Oops... There is an error on the server</span>');
                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }
            });
        });
        
    });
        $("#createmenu").attr("href", "/grades/create");
        $("#listmenu").attr("href", "/grades");
    </script>
@endsection
