@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Editar Calificación</span>
                            <div class="row">
                                <div class="row">
                                    {!! Form::model($grade, ['method' => 'PATCH','route' => ['grades.update', $grade->id],'class'=> 'col s12', 'role'=>'form']) !!}
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                        <input name="student" id="icon_prefix" type="text"
                                            value="{{ $grade->student->first_name }}" readonly>
                                            <label for="student">Alumno</label>
                                        </div>
                                        <div class="col s4">
                                        <label class="active" style="font-size:15px">Curso que califica</label>
                                        <select name="course" class="browser-default">
                                            <option value="" disabled>Seleccione el curso</option>
                                            <?php 
                                                foreach($courses as $course)
                                                {
                                                    if($course->id == $grade->course->id)
                                                        echo '<option value='.$course->id.' selected>'.$course->name.'</option>';
                                                    else
                                                    {
                                                        echo '<option value='.$course->id.'>'.$course->name.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            <input name="grade" id="grade" type="number" value="{{ $grade->grade }}">
                                            <label for="student">Calificación</label>
                                        </div>
                                        <div class="col s4">
                                            <label class="active" style="font-size:15px">Mes calificado</label>
                                            <select name="month" class="browser-default">
                                                <option value="" disabled>Seleccione el Mes</option>
                                                <?php for($x=1; $x<=$months_in_course; $x++)
                                                {
                                                    if($grade->month_number == $x)
                                                        echo '<option value="'.$x.'" selected>'.$x.'</option>';
                                                    else
                                                        echo '<option value="'.$x.'">'.$x.'</option>';
                                                }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::submit('Actualizar', array('class' => 'btn-block btn-large waves-effect waves-light green')) }}
                                        </div>
                                    </div>
                                    {{ Form::Close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/grades/create");
        $("#listmenu").attr("href", "/grades");
    </script>
@endsection
