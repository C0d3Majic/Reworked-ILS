@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Agregar un nuevo lenguaje</span>
                        <div class="row">
                            <form name="form1" class="col s12" method="POST" class="form-horizontal" role="form"
                                  action="{{ action('LanguagesController@store') }}" }}
                            ">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <br>
                            <h5 style="text-align: center">Datos del lenguaje</h5>
                            <div class="row">
                                <div class="input-field col s3">
                                    <input name="name" id="icon_prefix" type="text" value="">
                                    <label for="name">Nombre</label>
                                </div>
                                <div class="input-field col s3">
                                        <select name="branch">
                                            <option value="" disabled>Sucursal</option>
                                            @foreach($branches as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                
                                            @endforeach
                                        </select>
                                    <label>Sucursal</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s8">
                                    <input name="notes" id="icon_prefix" type="text" value="">
                                    <label for="notes">Notas</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <button class="btn-block btn-large waves-effect waves-light green"
                                            type="submit">Guardar
                                        <i class="material-icons right">check</i>
                                    </button>
                                </div>
                                <div class="col s10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $('.timepicker').daterangepicker(
                {
                    singleDatePicker: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    },
                },
                function (start, end, label) {
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });
        });

        $("#createmenu").attr("href", "/languages/create");
        $("#listmenu").attr("href", "/languages");

    </script>
@endsection
