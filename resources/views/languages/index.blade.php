@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
        .modal{
            max-height:18%;
        }
    </style>
    <div class="container">
        <!-- Left side of                                                                                                                                                                                                                                                                                                                                       the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s4">
                                <h5> Registro de Lenguajes</h5>
                                @if(Auth::user()->role == 1)
                                    <label>Select Branch</label>
                                    <select id="branches" class="browser-default">
                                    @foreach($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                    @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="languages" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Notas</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($languages as $language)
                                            <tr>
                                                <td>{{$language->id}}</td>
                                                <td>{{$language->name}}</td>
                                                <td>{{$language->notes}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Structure -->
        <div id="modal2" class="modal">
            <div class="modal-content">
                <h4>Eliminar lenguage</h4>
                <p>Estas seguro de querer eliminar este lenguaje?</p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col s6">
                        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>
                    </div>
                    <div class="col s6">
                        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            /*Ajax funtions for welcome page*/
            $(document).ready(function () {
                $('.modal').modal({dismissible: false} );
                $('select').material_select();
                var table = $('#languages').DataTable({
                    select: true,
                });

                table
                    .on('select', function (e, dt, type, indexes) {
                        var rowData = table.rows(indexes).data().toArray();
                        $selectedId = rowData[0][0]
                        $("#editmenu").attr("href", "/languages/" + $selectedId + "/edit");
                        $("#showmenu").attr("href", "/languages/" + $selectedId );
                    })

                $('#branches').on('change', function(){
                    $.ajax({
                    // The URL for the request. variable set above
                    url: "{{url('languages/change/branch')}}",
                    // The data to send (will be converted to a query string). variable set above
                    data: { branch_id: $('#branches').val() },
                    // Whether this is a POST or GET request
                    type: "POST",
                    // The type of data we expect back. can be json, html, text, etc...
                    dataType : "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // Code to run if the request succeeds;
                    // the response is passed to the function
                    success: function( msg ) {
                        if(msg.status == 'error'){
                            var $toastContent = $('<span>'+ msg.type +'</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }else if(msg.status == 'success'){
                            console.log(msg.students);
                            var totalObjetos = (msg.students).length;
                            var contObjetos = 0;
                            //------------------------------------- CODE TO UPDATE THE DATATABLE ------------
                            //table.rows().remove(); //We delete the all the rows to update the datatable
                            table.rows().remove();
                            for (contObjetos = 0; contObjetos < totalObjetos; contObjetos++) 
                                table.row.add([msg.students[contObjetos].id,msg.students[contObjetos].name,msg.students[contObjetos].notes]);
                            table.draw();
                        }
                    },
                    error: function(){
                        //window.location.reload();
                        var $toastContent = $('<span>Oops... There is an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                    });
                });

                $('#deletemenu').click(function(){
                    $.ajax({
                    // The URL for the request. variable set above
                    url: "{{url('language/retrieve')}}",
                    // The data to send (will be converted to a query string). variable set above
                    data: { language_id: $selectedId },
                    // Whether this is a POST or GET request
                    type: "POST",
                    // The type of data we expect back. can be json, html, text, etc...
                    dataType : "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // Code to run if the request succeeds;
                    // the response is passed to the function
                    success: function( msg ) {
                        if(msg.status == 'error'){
                            var $toastContent = $('<span>'+ msg.type +'</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }else if(msg.status == 'success'){
                            $('.modal-content').html(msg.modal);
                            $('#modal2').modal('open',{dismissible:false});
                        }
                    },
                    error: function(){
                        //window.location.reload();
                        var $toastContent = $('<span>Oops... There is an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                    });
                });

                $('#btnEliminar').click(function(){
                    $.ajax({
                        url: '/languages/' + $selectedId,
                        type: 'DELETE',
                        data: {'_token': '{!! csrf_token() !!}'},
                        async: false,
                        success: function (result) {
                            console.log(result);
                            if (result == "success") {
                                Materialize.toast('<span>Course deleted successfully</span>', 2000, 'rounded green darken-3');
                                setTimeout(function () {// wait for 5 secs(2)
                                    location.reload();
                                }, 1000);
                            }else
                            {
                                var $toastContent = $('<span>There was an error deleting the course</span>');
                                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                            }
                        }
                    });
                });
            });

            $("#createmenu").attr("href", "/languages/create");
            $("#listmenu").attr("href", "/languages");

        </script>

        <!--
          Content Section End
        -->
@endsection

