@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Ver lenguaje</span>
                        <div class="row">
                            <form action="{{ route('LanguagesController.update', $language->id) }}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <h5 style="text-align: center">Datos del lenguaje</h5>
                                <div class="row">
                                    <div class="input-field col s3">
                                        <input name="name" id="icon_prefix" type="text" value="{{ $language->name }}">
                                        <label for="name">Nombre</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s8">
                                        <input name="notes" id="icon_prefix" type="text" value="{{ $language->notes }}">
                                        <label for="notes">Notas</label>
                                    </div>
                                </div>
                                <div class="row">
                                    {{--<div class="input-field col s4">--}}
                                        {{--<button class="btn-block btn-large waves-effect waves-light green"--}}
                                                {{--type="submit">Actualizar--}}
                                            {{--<i class="material-icons right">check</i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    <div class="col s10">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("input, textarea, select").prop("readonly", true);
        });

        $("#createmenu").attr("href", "/languages/create");
        $("#listmenu").attr("href", "/languages");

    </script>
@endsection
