@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
    </style>
    <div class="container">
        <!-- Left side of                                                                                                                                                                                                                                                                                                                                       the view AKA THE CRUD MENU -->
    @include("partials.crudmenu3")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s4">
                                <h5> Listado de logs</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="log" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Tipo</th>
                                            <th>Contenido Id</th>
                                            <th>Tabla</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($log as $l)
                                            <tr>
                                                <td>{{ $l->id }} </td>
                                                <td>{{ $l->date_time }}</td>
                                                <td>{{ \App\User::find($l->user_id)->name }}</td>
                                                <td>{{ $l->type }}</td>
                                                <td>{{ $l->content_id  }}</td>
                                                <td>{{ $l->table_name  }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            /*Ajax funtions for welcome page*/
            $(document).ready(function () {

                var table = $('#log').DataTable({
                    select: true,
                });

                table
                    .on('select', function (e, dt, type, indexes) {
                        var rowData = table.rows(indexes).data().toArray();
                        $selectedId = rowData[0][0]
                        $("#editmenu").attr("href", "/log/" + $selectedId + "/edit");
                        $("#showmenu").attr("href", "/log/" + $selectedId );
                    })

            });

        </script>

        <!--
          Content Section End
        -->
@endsection

