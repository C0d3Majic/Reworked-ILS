@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu3")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Ver log</span>
                        <div class="row">
                            <br>
                            <h5 style="text-align: center">Detalles del log</h5>
                            <div class="row">
                                <div class="input-field col s2 right">
                                    <input name="name" id="icon_prefix" type="text" value="{{ $log->id }}">
                                    <label for="name">Id</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <input name="notes" id="icon_prefix" type="text" value="{{ $log->date_time }}">
                                    <label for="notes">Fecha</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="notes" id="icon_prefix" type="text"
                                           value="{{ \App\User::find($log->user_id)->name }}">
                                    <label for="notes">Usuario</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <input name="notes" id="icon_prefix" type="text" value="{{ $log->type }}">
                                    <label for="notes">Tipo</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="notes" id="icon_prefix" type="text" value="{{ $log->table_name }}">
                                    <label for="notes">Tabla</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="notes" id="icon_prefix" type="text" value="{{ $log->content_id }}">
                                    <label for="notes">Contenido Id</label>
                                </div>
                            </div>
                            @if($log->type=='update')
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea name="notes" id="icon_prefix" type="text"
                                                  style="width:49%;height:300px;">{{  str_replace(",", "\n", $log->content) }}</textarea>
                                        <textarea name="notes" id="icon_prefix" type="text"
                                                  style="width:49%;height:300px;">{{  str_replace(",", "\n", $log->content) }}</textarea>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea name="notes" id="icon_prefix" type="text"
                                                  style="width:100%;height:300px;">{{  str_replace(",", "\n", $log->content) }}</textarea>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("input, textarea, select").prop("readonly", true);
        });

        $("#listmenu").attr("href", "/log");

    </script>
@endsection
