<script>

    /*Ajax funtions for welcome page*/
    $(document).ready(function () {
        $('.modal').modal({dismissible: false} );
        $(".dropdown-button").dropdown();
        $('select').material_select();
        $selectedId = 0;
        var machines = $('#machines').DataTable({
            select: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'Ir a seguimiento',
                    action: function () {
                        url = "http://kaizenprolaravel.dev:8081/alerts/" + $selectedFolio + "/edit";
                        window.location = url;
                    }
                }
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                "select": {
                    rows: " / %d selecionado(s)"
                },
            },
            responsive: true,
            "columnDefs": [
                {"width": "10%", "targets": 0}
            ]
        });

        $selectedFolio = 0;
        machines
            .on('select', function (e, dt, type, indexes) {
                var rowData = machines.rows(indexes).data().toArray();
                $selectedFolio = rowData[0][0]
            })

        $('.carousel').carousel({
            duration: 200
        });

        var alerts = $('#alerts').DataTable({
            //changeLenght : true,
            //lengthMenu: [ [100,250, -1], [100,250, "All"] ],
            //order: [ 5, 'asc' ],
            //iDisplayLength: 50,
            //"sDom": '<"right"l>rt<"bottom"flp><"clear">',
            //sDom": 'lTfrtip',
            buttons: true,
            select: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                "select": {
                    rows: " / %d selecionado(s)"
                },
            },
            "order": [[0, "desc"]],
            responsive: true,
            "columnDefs": [
                {"width": "10%", "targets": 0}
            ]
        }); //Datatable instantiation
        var table = $('#alerts').DataTable();
        table.column(0).visible(false);
        $('select').material_select();

        alerts
            .on('select', function (e, dt, type, indexes) {
                var rowData = alerts.rows(indexes).data().toArray();
                $selectedId = rowData[0][0]
                $("#editmenu").attr("href", "/students/" + $selectedId + "/edit");
                $("#showmenu").attr("href", "/students/" + $selectedId );
            })

            $('#branches').on('change', function(){
            $.ajax({
                    // The URL for the request. variable set above
                    url: "{{url('students/change/branch')}}",
                    // The data to send (will be converted to a query string). variable set above
                    data: { branch_id: $('#branches').val() },
                    // Whether this is a POST or GET request
                    type: "POST",
                    // The type of data we expect back. can be json, html, text, etc...
                    dataType : "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // Code to run if the request succeeds;
                    // the response is passed to the function
                    success: function( msg ) {
                        if(msg.status == 'error'){
                            var $toastContent = $('<span>'+ msg.type +'</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }else if(msg.status == 'success'){
                            console.log(msg.students);
                            var totalObjetos = (msg.students).length;
                            var contObjetos = 0;
                            //------------------------------------- CODE TO UPDATE THE DATATABLE ------------
                            //table.rows().remove(); //We delete the all the rows to update the datatable
                            table.rows().remove();
                            for (contObjetos = 0; contObjetos < totalObjetos; contObjetos++) 
                                table.row.add([msg.students[contObjetos].id,msg.students[contObjetos].controlNumber,msg.students[contObjetos].last_name,msg.students[contObjetos].first_name]);
                            table.draw();
                        }
                    },
                    error: function(){
                        //window.location.reload();
                        var $toastContent = $('<span>Oops... There is an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                    });
                });

                $('#deletemenu').click(function(){
                    $.ajax({
                    // The URL for the request. variable set above
                    url: "{{url('student/retrieve')}}",
                    // The data to send (will be converted to a query string). variable set above
                    data: { student_id: $selectedId },
                    // Whether this is a POST or GET request
                    type: "POST",
                    // The type of data we expect back. can be json, html, text, etc...
                    dataType : "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // Code to run if the request succeeds;
                    // the response is passed to the function
                    success: function( msg ) {
                        if(msg.status == 'error'){
                            var $toastContent = $('<span>'+ msg.type +'</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }else if(msg.status == 'success'){
                            $('.modal-content').html(msg.modal);
                            $('#modal2').modal('open',{dismissible:false});
                        }
                    },
                    error: function(){
                        //window.location.reload();
                        var $toastContent = $('<span>Oops... There is an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                    });
                });

                $('#btnEliminar').click(function(){
                    $.ajax({
                        url: '/students/' + $selectedId,
                type: 'DELETE',
                data: {'_token': '{!! csrf_token() !!}'},
                async: false,
                success: function (result) {
                    console.log(result);
                    if (result == "success") {
                                Materialize.toast('<span>Course deleted successfully</span>', 2000, 'rounded green darken-3');
                                setTimeout(function () {// wait for 5 secs(2)
                            location.reload();
                        }, 1000);
                            }else
                            {
                                var $toastContent = $('<span>There was an error deleting the course</span>');
                                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }
                }
            });
        });
        $('input[name="daterange"]').daterangepicker(
            {
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: '2017-01-01',
                endDate: '2017-12-31'
            },
            function (start, end, label) {
                alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
    })
    ;
</script>
