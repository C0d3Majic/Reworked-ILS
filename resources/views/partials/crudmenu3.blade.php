<div class="row">
    <div class="col s2 hidden-xs">
        <ul class="vertical_nav_ul">
            <br><br>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href=""></a>
            </li>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href=""></a>
            </li>
            <li class="vertical_nav_li_a"><a id="listmenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href="/students">Lista<br><i
                            class="large material-icons">list</i></a>
            </li>
            <li class="vertical_nav_li_a"><a id="showmenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href="/students">Ver<br><i
                            class="large material-icons">pageview</i></a>
            </li>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href=""></a>
            </li>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0C1959" href=""></a>
            </li>
            <br><br>
        </ul>
    </div>
    <div class="col s2 visible-xs">
        <ul class="vertical_nav_ul">
            <br><br>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978" href=""></a>
            </li>
            <li class="vertical_nav_li_a"><a id="createmenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978" href="/students/create"><br><i
                            class="small material-icons">queue</i></a>
            </li>
            <li class="vertical_nav_li_a"><a id="editmenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978" href=""><br><i
                            class="small material-icons">edit</i></a>
            </li>
            <li class="vertical_nav_li_a"><a id="deletemenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978"><br><i
                            class="small material-icons">delete</i></a>
            </li>
            <li class="vertical_nav_li_a"><a id="listmenu" class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978" href="/students"><br><i
                            class="small material-icons">list</i></a>
            </li>
            <li class="vertical_nav_li_a"><a class="vertical_nav_li_a"
                                             style="color:white; background-color:#0b5978" href=""></a>
            </li>
        </ul>
    </div>
