<script>
	/*Ajax funtions for welcome page*/
  $(document).ready(function(){
    $('select').material_select();
    $( "#studentNumber" ).focusout(function() {
        var jsonObject = {
          controlNumber : $("#studentNumber").val()
        }
        // Start $.ajax() method
        $.ajax({
            // The URL for the request. variable set above
            url: "{{url('retrieve/student/courses')}}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // The data to send (will be converted to a query string). variable set above
            data: jsonObject,
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
                if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success')
                {
                    if(msg.courses.length==1)
                    {
                        $('#courses').empty();
                        $('#courses').append('<option selected value="' + msg.courses[0].id + '">' + msg.courses[0].name + '</option>');
                    }
                    else {
                        console.log(msg.courses);
                        $('#courses').empty();
                        $('#courses').append('<option value="" disabled selected>Selecciona tu curso</option>');
                        for (i = 0; i < msg.courses.length; i++)
                            $('#courses').append('<option value="' + msg.courses[i].id + '">' + msg.courses[i].name + '</option>');
                    }
                    $('select').material_select();

                    // var formTeachers = document.getElementById("teacher_attendance_form")
                    // if(formTeachers) formTeachers.submit();

                    //var formStudents = document.getElementById("attendance_form");
                    //if(formStudents) formStudents.submit();
                }
            },
            error: function(){
              var $toastContent = $('<span>There was an error on the server</span>');
              Materialize.toast($toastContent, 2000, 'rounded red darken-3');
            }
        });
        return false;
    })

    $('select').material_select();
    $( "#teacherNumber" ).focusout(function() {
        var jsonObject = {
          controlNumber : $("#teacherNumber").val()
        }
        // Start $.ajax() method
        $.ajax({
            // The URL for the request. variable set above
            url: "{{url('retrieve/teacher/courses')}}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // The data to send (will be converted to a query string). variable set above
            data: jsonObject,
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
                if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success')
                {
                    console.log(msg.courses);
                    $('#teacherCourses').empty();
                    $('#teacherCourses').append('<option value="" disabled selected>Selecciona tu curso</option>');
                    for(i=0; i<msg.courses.length; i++)
                        $('#teacherCourses').append('<option value="'+msg.courses[i].id+'">'+msg.courses[i].name +'</option>');
                    $('select').material_select();
                }
            },
            error: function(){
              var $toastContent = $('<span>There was an error on the server</span>');
              Materialize.toast($toastContent, 2000, 'rounded red darken-3');
            }
        });
        return false;
    })

    $("#attendance_form").submit(function(){
        var controlNumber = $("#studentNumber").val();
        var course_id = $("#courses").val()
       // Start $.ajax() method
       $.ajax({
         // The URL for the request. variable set above
         url: $(this).attr('action'),
         // The data to send (will be converted to a query string). variable set above
         data: $(this).serialize(),
         // Whether this is a POST or GET request
         type: "POST",
         // The type of data we expect back. can be json, html, text, etc...
         dataType : "json",
         // Code to run if the request succeeds;
         // the response is passed to the function
         success: function( msg ) {
           if(msg.status == 'error'){
             var $toastContent = $('<span>'+ msg.type +'</span>');
             Materialize.toast($toastContent, 2000, 'rounded red darken-3');
           }else if(msg.status == 'success')
           {
            Materialize.toast('<span>Tu asistencia fue capturada correctamente</span', 2000, 'rounded green darken-3');
            var controlNumber = $("#studentNumber").val();
            location.href = "{{url('student/payment/info')}}/"+controlNumber+"/"+course_id;
            //setTimeout(function(){ window.location.reload(); }, 3000);

           }
         },
         error: function(){
           var $toastContent = $('<span>There was an error on the server</span>');
           Materialize.toast($toastContent, 2000, 'rounded red darken-3');
         }
       });
       return false;
     });

     $("#teacher_attendance_form").submit(function(){
       // Start $.ajax() method
       $.ajax({
         // The URL for the request. variable set above
         url: $(this).attr('action'),
         // The data to send (will be converted to a query string). variable set above
         data: $(this).serialize(),
         // Whether this is a POST or GET request
         type: "POST",
         // The type of data we expect back. can be json, html, text, etc...
         dataType : "json",
         // Code to run if the request succeeds;
         // the response is passed to the function
         success: function( msg ) {
           if(msg.status == 'error'){
             var $toastContent = $('<span>'+ msg.type +'</span>');
             Materialize.toast($toastContent, 2000, 'rounded red darken-3');
           }else if(msg.status == 'success')
           {
            Materialize.toast('<span>Tu asistencia fue capturada correctamente</span', 2000, 'rounded green darken-3');
            setTimeout(function(){ window.location.reload(); }, 3000);;
           }
         },
         error: function(){
           var $toastContent = $('<span>There was an error on the server</span>');
           Materialize.toast($toastContent, 2000, 'rounded red darken-3');
         }
       });
       return false;
     });
  });
</script>
