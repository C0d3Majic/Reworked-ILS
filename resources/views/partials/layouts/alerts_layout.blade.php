<!DOCTYPE html>
<html lang="en">
    <head>
      @include("partials.headers")
      @include("partials.styles_css.general_styles")
    </head>
    <body>
      @yield("content")
      @include("partials.scripts.general_scripts")
			@include("partials.ajax.alerts_ajax")
    </body>
</html>
