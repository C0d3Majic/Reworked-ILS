<!DOCTYPE html>
<html lang="en">
    <head>
      @include("partials.headers")
      @include("partials.styles_css.attendance_styles")
    </head>
    <body id="body" class="transparent">
      @yield("content")

      @include("partials.scripts.general_scripts")
			@include("partials.jquery.attendance_jquery")
    </body>
</html>
