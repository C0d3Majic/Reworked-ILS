<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        @media (orientation:landscape ) {
            .playfield { display: block; }
            .overlay { display: none; }
        }
        @media (orientation: portrait ) {
            .playfield { display: none; }
            .overlay { display: block; }
        }
    </style>
    <link rel='stylesheet' href='css/fullcalendar.css' />
    <link rel='stylesheet' href='css/materializefullcalendar.css' />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kaizen Pro') }}</title>

    @include("partials.headers")
    @include("partials.styles_css.general_styles")

</head>
<body>

<!-- Dropdown Structure for Catalogos button-->
<ul id='dropdownMantenimiento' class='nav navbar-nav dropdown-content'>
    <li><a class="horizontal_nav_li_a" href="{{url('students')}}">Alumnos</a></li>
    <li><a class="horizontal_nav_li_a" href="{{url('teachers')}}">Profesores</a></li>
    <li><a class="horizontal_nav_li_a" href="{{url('courses')}}">Cursos</a></li>
    <li><a class="horizontal_nav_li_a" href="{{url('languages')}}">Lenguajes</a></li>
</ul>

<ul id='dropdownRelationships' class='nav navbar-nav dropdown-content'>
    <li><a class="horizontal_nav_li_a" href="{{url('studentAttendances')}}">Alumnos</a></li>
    <li><a class="horizontal_nav_li_a" href="{{url('teacherAttendances')}}">Profesores</a></li>
</ul>

<!-- Dropdown Structure for Sistema button-->
@if(Auth::User()->role == 1)
    <ul id='dropdownSistema' class='nav navbar-nav dropdown-content'>
        <li><a class="horizontal_nav_li_a" href="{{url('users')}}">Usuarios</a></li>
        <li><a class="horizontal_nav_li_a" href="{{url('billing')}}">Pagos</a></li>
        <li><a class="horizontal_nav_li_a" href="{{url('log')}}">Log Manager</a></li>
        <li><a class="horizontal_nav_li_a" href="{{url('calendar')}}">Calendario</a></li>
    </ul>
@endif

<div class="overlay">
    <h2 align="center">Favor de rotar tu dispositivo.</h2>
    <img src="/img/rotate_icon_ipad.png" width="100%" height="100%" />
</div>

<div id="app" class="playfield">
    <nav class="navbar navbar-default navbar-static-top" style="height:120px;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/students') }}">
                    {{--{{ config('app.name', 'Kaizen Pro') }}--}}
                    <img class="responsive-img" src="/img/logo_ils.png" width="80%">
                </a>

            </div>

            {{--<div class="collapse navbar-collapse visible-lg visible-md visible-sm" id="app-navbar-collapse">--}}
            <div class="collapse navbar-collapse" id="app-navbar-collapse">

                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="horizontal_nav_li_a"><a class='horizontal_nav_li_a dropMenu dropdown-button'
                                                       data-beloworigin="true" data-activates='dropdownMantenimiento'
                                                       data-trigger='click' data-alignment="left" href="/">Catálogos</a>
                    </li>
                    <li class="horizontal_nav_li_a"><a class='horizontal_nav_li_a dropMenu dropdown-button'
                                                       data-beloworigin="true" data-activates='dropdownRelationships'
                                                       data-trigger='click' data-alignment="left" href="/">Asistencias</a>
                    </li>
                    @if(Auth::User()->role == 1)
                        <li class="horizontal_nav_li_a"><a class='horizontal_nav_li_a dropMenu dropdown-button'
                                                           data-beloworigin="true" data-activates='dropdownSistema'
                                                           data-trigger='click' data-alignment="left" href="/">Sistema</a>
                        </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
@include("partials.scripts.general_scripts")
{{--@include("partials.ajax.alerts_ajax")--}}

</body>
<footer>
    <script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
    <div align="center">
        <span style="font-weight:bold">
            {{--© 2017 REPINEL ALL RIGHTS RESERVED<br>--}}
            {{--<a href="http:\\www.repinel.com.mx">Repinel.com.mx</a><br>--}}
        </span><br>
    </div>
</footer>

</html>