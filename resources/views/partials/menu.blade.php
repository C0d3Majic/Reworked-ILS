<!--
  Header Section Start
-->
<div class="navbar-fixed">
  <nav class="" style="background-color:#b71c1c;">
    <div class="nav-wrapper">
			@if (Auth::check())
      <div href="#" class="brand-logo center">Fuxs CMS</div>
			@else
			<div href="#" class="brand-logo left space-div">Fuxs CMS</div>
			@endif
			@if (Auth::check())
			<ul id="slide-out" class="side-nav">
				<li><div class="user-info">Hi, {{ $user->name }}</div></li>
				<li><a href="{{ URL::to('/') }}"><i class="material-icons">place</i>Places</a></li>
				<li><a href="{{ URL::to('view/categories') }}"><i class="material-icons">label</i>Categories</a></li>
				<li><a href="{{ URL::to('view/users') }}"><i class="material-icons">people</i>Users</a></li>
				<li><a href="{{ URL::to('view/reviews') }}"><i class="material-icons">start_rate</i>Review</a></li>
		    <li><a id="to_logout" class="log-out"><i class="material-icons">exit_to_app</i>Log Out</a></li>
	    </ul>
    	<a id="menu_nav" href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="material-icons">menu</i></a>
		@endif
		</div>
  </nav>
</div>
<!--
  Header Section End
-->
