<!-- ================================================
  Scripts
  ================================================ -->

<!--Import Bootstrap-->
{{--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>--}}
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
{{--<script type="text/javascript" src="/js/jquery-ui.min.js"></script>--}}
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css" />

<!-- Include FullCallendar -->
<script src='/js/moment.min.js'></script>
<script src='/js/fullcalendar.js'></script>
<script src='/js/locale-all.js'></script>

<!-- Include Time Picker -->
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="/css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="/js/jquery.ptTimeSelect.js"></script>

<!-- Include Date Tables -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!--bootbox js-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<!--materialize js-->
<script type="text/javascript" src="/js/materialize.min.js"></script>

<!-- Google Charts -------------->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- QCode-Decoder-------------->
<script type="text/javascript" src="/js/qrcode.min.js"></script>




