<style>
	html, body{
    height:100%;
    width: 100%;
	}
	.container{
	    width:100%;
	    height:100%;
			padding-left: 0px;
	}
	main {
		flex: 1 0 auto;
	}

	.vertical_nav_ul{
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	    width: 100%;
			text-align: center;
			vertical-align: middle !important;
	}

  .vertical_nav_li_a
  {
	    display: block;
			height: 120px;
	    background-color: #ffffff;
			text-align: center;
			vertical-align: middle !important;
	}

	.horizontal_nav_ul {
		list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
	}

	.horizontal_nav_li_a{
				float: left;
		    display: block;
		    padding:30px;
		    color: #dddddd;
				font-size: 25px;
	}

	.dropdown-content {
   	overflow: visible !important;
	}

	.dropdown-content .dropdown-content {
	   margin-left: 100%;
	}

	.dropMenu {
		padding: 14px 16px !important;
		width: 170px;
		line-height: 15px !important;
	}
	.dropMenu:hover{
		background: #fff;
		color: #777;
	}

	.dropdown-content li>a,
		.dropdown-content li>span {
	    font-size: 16px;
	    color: #777;
	    display: block;
	    background-color: #e7e7e7;
			clear: both;
	    cursor: pointer;
	    min-height: 0px;
	    line-height: 0rem;
	    width: 100%;
	    text-align: center;
	    text-transform: none;
	}
	.dropdown-content li {
	    min-height: 0px;
	}

    .bootbox {
        /*left: auto !important;*/
        /*width: auto !important;*/
        /*margin-left: auto !important;*/
        background-color: transparent !important;
        border: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        /*position: relative !important;*/
        /*top: 50% !important;*/
        overflow: visible !important;
        transition: opacity 0.15s linear 0s !important;
    }

</style>
