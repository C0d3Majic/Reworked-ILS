@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Modificar fecha y/o hora de asistencia</span>
                        <div class="row">
                            <form action="{{ route('StudentAttendancesController.update', $studentAttendance->id) }}"
                                  method="POST">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="student" id="icon_prefix" type="text"
                                               value="{{ $studentAttendance->name }}" readonly>
                                        <label for="student">Alumno</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="course" id="icon_prefix" type="text"
                                               value="{{ $studentAttendance->course }}" readonly>
                                        <label for="course">Curso</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="attendance_date" id="icon_prefix" type="text" class="datepicker"
                                               value="">
                                        <label for="attendance_date">Fecha en la que se registra la asistencia</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="attendance_time" id="icon_prefix" type="text" class="timepicker"
                                               value="">
                                        <label for="attendance_time">Hora en la que se registra la asistencia</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <button class="btn-block btn-large waves-effect waves-light green"
                                                type="submit">Actualizar
                                            <i class="material-icons right">check</i>
                                        </button>
                                    </div>
                                    <div class="col s10">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('.datepicker').daterangepicker(
                {
                    singleDatePicker: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    },
                },
                function (start, end, label) {
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });

            $('.timepicker').timepicker({'timeFormat': 'H:i'});
        });

        $("#createmenu").attr("href", "/studentAttendances/create");
        $("#listmenu").attr("href", "/studentAttendances");

    </script>
@endsection
