@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
    </style>
    <div class="container">
        <!-- Left side of                                                                                                                                                                                                                                                                                                                                       the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s4">
                                <h5> Registros de Asistencia</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="attendances" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Alumno</th>
                                            <th>Curso</th>
                                            <th>Fecha y Hora</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($studentAttendances as $studentAttendance)
                                            <tr>
                                                <td>{{ $studentAttendance->id}}</td>
                                                <td>{{ $studentAttendance->name }}</td>
                                                <td>{{ $studentAttendance->course }}</td>
                                                <td>{{ $studentAttendance->attendance_datetime}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <h5>Reportes de asistencia por curso</h5>
                                <div class="col s6">
                                    <label class="active">Cursos Activos</label>
                                    <select id="courses">
                                        <option value="" disabled selected>Escoje el curso</option>
                                        @foreach($courses as $course)
                                            <option value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                                <div class="input-field col s6">
                                    <a id="reporte" class="waves-effect waves-light btn-large"><i class="material-icons right">assignment</i>Generar Reporte</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            /*Ajax funtions for welcome page*/
            $(document).ready(function () {
                $('#reporte').click(function(){
                    var course_id;
                    course_id = $("#courses").val();
                    if(course_id == null || course_id == "")
                        Materialize.toast('<span>Falta escojer un curso</span>', 2000, 'rounded red darken-3');
                    else
                        window.open("{{url('students/attendance_report')}}/"+course_id,"_blank");
                });
                var table = $('#attendances').DataTable({
                    select: true,
                });

                table
                    .on('select', function (e, dt, type, indexes) {
                        var rowData = table.rows(indexes).data().toArray();
                        $selectedId = rowData[0][0]
                        $("#editmenu").attr("href", "/studentAttendances/" + $selectedId + "/edit");
                    })

                $("#deletemenu").click(function () {
                    $.ajax({
                        url: '/codes/' + $selectedId,
                        type: 'DELETE',
                        data: {'_token': '{!! csrf_token() !!}'},
                        async: false,
                        success: function (result) {
                            console.log(result);
                            if (result == "success") {
                                setTimeout(function () {// wait for 5 secs(2)
                                    location.reload();
                                }, 1000);
                            }
                        }
                    });
                });
            });

            $("#createmenu").attr("href", "/studentAttendances/create");
            $("#listmenu").attr("href", "/studentAttendances");

        </script>

        <!--
          Content Section End
        -->
@endsection
