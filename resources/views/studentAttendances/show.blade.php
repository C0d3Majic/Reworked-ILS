@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Ver cliente</span>
                        {{ Form::model($alert, array('action' => array('AlertsController@show', $alert->id), 'method' => 'GET')) }}
                        <br>
                        <div class="row">
                            <div class="input-field col s8">
                                {{ Form::text('name', $alert->name, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('name', 'Nombre', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                            <div class="input-field col s2">
                                {{ Form::text('phone', $alert->phone, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('phone', 'Telefono', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('visit_date', $alert->visit_date, array('style' => 'font-size:15px;', 'class' => 'timepicker', 'readonly' => 'true')) }}
                                {!! Form::Label('visit_date', 'Fecha de la proxima visita', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('birthdate', $alert->birthdate, array('style' => 'font-size:15px;', 'class' => 'timepicker', 'readonly' => 'true')) }}
                                {!! Form::Label('birthdate', 'Fecha de nacimiento', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('email', $alert->email, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('email', 'Correo electronico', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('attendant', $alert->attendant, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('attendant', 'Estilista', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('service', $alert->service, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('service', 'Servicio', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {{ Form::text('observations', $alert->observations, array('style' => 'font-size:15px;', 'readonly' => 'true')) }}
                                {!! Form::Label('observations', 'Observaciones', array('style' => 'font-size:15px;', 'class' => 'active' )) !!}
                            </div>
                        </div>
                    </div>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection
