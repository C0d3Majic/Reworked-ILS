@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Agregar un nuevo alumno</span>
                        <div class="row">
                            <form name="form1" class="col s12" method="POST" class="form-horizontal" role="form"
                                  action="{{ action('StudentsController@store') }}" }}
                            ">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <br>
                            <div class="row">
                                <div class="input-field col s4 right">
                                    <input name="registration_date" id="icon_prefix" type="text" class="timepicker"
                                           value="">
                                    <label for="comments">Fecha de Inscripción</label>
                                </div>
                            </div>
                            <h5 style="text-align: center">Datos del alumno</h5>
                            <div class="row">
                                <div class="input-field col s3">
                                    <input name="last_name" id="icon_prefix" type="text" value="">
                                    <label for="comments">Apellido(s)</label>
                                </div>
                                <div class="input-field col s3">
                                    <input name="first_name" id="icon_prefix" type="text" value="">
                                    <label for="comments">Nombre(s)</label>
                                </div>
                                <!--Added Matricula field for attendance logic -->
                                {{--<div class="input-field col s1">--}}
                                    {{--<input name="controlNumber" id="icon_prefix" type="number" value="">--}}
                                    {{--<label for="controlNumber">Matricula</label>--}}
                                {{--</div>--}}
                                <div class="input-field col s2">
                                    <select name="genre">
                                        <option value="" selected>Género</option>
                                        <option value="Male">Masculino</option>
                                        <option value="Female">Femenino</option>
                                    </select>
                                </div>
                                <div class="input-field col s4">
                                        <select name="branch">
                                            <option value="" disabled>Sucursal</option>
                                            @foreach($branches as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                
                                            @endforeach
                                        </select>
                                    <label>Sucursal</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s2">
                                    <input name="birthdate" id="birthdate" type="text" class="timepicker" value="">
                                    <label for="comments">Fecha de Nacimiento</label>
                                </div>
                                <div class="input-field col s1">
                                    <input name="age" id="age" type="text" value="">
                                    <label for="comments">Edad</label>
                                </div>
                                <div class="input-field col s5">
                                    <input name="ocupation" id="icon_prefix" type="text" value="">
                                    <label for="comments">Ocupación</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="billing_email" id="icon_prefix" type="text" value="">
                                    <label for="comments">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s8">
                                    <input name="address" id="icon_prefix" type="text" value="">
                                    <label for="comments">Domicilio</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="phone" id="icon_prefix" type="text" value="">
                                    <label for="comments">Teléfono</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input name="company" id="icon_prefix" type="text" value="">
                                    <label for="comments">Compañía Representante</label>
                                </div>
                                <div class="input-field col s6">
                                    <input name="institution" id="icon_prefix" type="text" value="">
                                    <label for="comments">Institución Educativa</label>
                                </div>
                            </div>
                            <h5 style="text-align: center">Datos de facturación</h5>
                            <div class="row">
                                <div class="input-field col s4">
                                    <input name="payment_type" id="icon_prefix" type="text" value="">
                                    <label for="comments">Tipo de pago</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="business_name" id="icon_prefix" type="text" value="">
                                    <label for="comments">Razón social</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="RFC" id="icon_prefix" type="text" value="">
                                    <label for="comments">RFC</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s8">
                                    <input name="billing_address" id="icon_prefix" type="text" value="">
                                    <label for="comments">Dirección</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="billing_postal_code" id="icon_prefix" type="text" value="">
                                    <label for="comments">Código postal</label>
                                </div>
                            </div>
                            <div id="adultInfo" style="display: none;">
                                <h5 style="text-align: center">En caso de una emergencia contactar</h5>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <input name="emergency_contact_name" id="icon_prefix" type="text" value="">
                                        <label for="comments">Nombre y apellido</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="emergency_contact_phone" id="icon_prefix" type="text" value="">
                                        <label for="comments">Tel celular</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="emergency_contact_relationship" id="icon_prefix" type="text" value="">
                                        <label for="comments">Parentesco</label>
                                    </div>
                                </div>
                                <h5 style="text-align: center">Como se entero de ILS</h5>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="how_did_you_find_out" id="icon_prefix" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div id="childInfo" style="display: none;">
                                <h5 style="text-align: center">Datos del padre</h5>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <input name="parent1_name" id="icon_prefix" type="text" value="">
                                        <label for="comments">Nombres y apellidos del padre</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="parent1_phone" id="icon_prefix" type="text" value="">
                                        <label for="comments">Tel celular</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="parent1_email" id="icon_prefix" type="text" value="">
                                        <label for="comments">Correo electrónico</label>
                                    </div>
                                </div>
                                <h5 style="text-align: center">Datos de la madre</h5>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <input name="parent2_name" id="icon_prefix" type="text" value="">
                                        <label for="comments">Nombres y apellidos del padre</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="parent2_phone" id="icon_prefix" type="text" value="">
                                        <label for="comments">Tel celular</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="parent2_email" id="icon_prefix" type="text" value="">
                                        <label for="comments">Correo electrónico</label>
                                    </div>
                                </div>
                                <h5 style="text-align: center">Comentarios que necesiten ser de conocimiento para la
                                    escuela y profesor(a).
                                    (Alergías, actitudes, enfermedades, taquicardia, situaciones importantes,
                                    discapacidades, etc.)</h5>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="sufferings" id="icon_prefix" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <h5 style="text-align: center">Curso(s) en el que alumno se inscribe</h5>
                            <div class="row">
                                <div style="display: visible">
                                    <table id="courses" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Lenguaje</th>
                                            <th>Nivel</th>
                                            <th>Profesor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($courses as $course)
                                            <tr>
                                                <td>{{$course->id}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->language->name }}</td>
                                                <td>{{$course->level}}</td>
                                                @if($course->teacher)
                                                    <td>{{$course->teacher->last_name.' '.$course->teacher->first_name}}</td>
                                                @else
                                                    <td>NA</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <input type="hidden" name="selectedCoursesIds" id="selectedCoursesIds" value=""/>
                            {{--<label for="selectedCoursesIds">Ids de cursos</label>--}}
                            <div class="row">
                                <div class="input-field col s4">
                                    <button class="btn-block btn-large waves-effect waves-light green"
                                            type="submit">Guardar
                                        <i class="material-icons right">check</i>
                                    </button>
                                </div>
                                <div class="col s10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            var table = $('#courses').DataTable({
                select: {
                    style: 'multi'
                }
            });

            table
                .on('select', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table.rows({selected: true}).data().toArray();

                    $('#selectedCoursesIds').val('');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedCoursesIds').val(rowData[i][0]);
                        else
                            $('#selectedCoursesIds').val($('#selectedCoursesIds').val() + ',' + rowData[i][0]);
                    }
                })

            table
                .on('deselect', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table.rows({selected: true}).data().toArray();

                    $('#selectedCoursesIds').val('');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedCoursesIds').val(rowData[i][0]);
                        else
                            $('#selectedCoursesIds').val($('#selectedCoursesIds').val() + ',' + rowData[i][0]);
                    }
                })

            $("#age").change(function () {
                if ($("#age").val() >= 18) {
                    $("#adultInfo").css("display", "block");
                    $("#childInfo").css("display", "none");
                }
                else {
                    $("#adultInfo").css("display", "none");
                    $("#childInfo").css("display", "block");
                }
            });

            $('.timepicker').daterangepicker(
            {
                singleDatePicker: true,
                locale: {
                    format: 'YYYY-MM-DD'
                },
            },
            function (start, end, label) {
                //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });

            $("#birthdate").focusout(function() {
                $("#age" ).val(getAge($("#birthdate").val()));
            })

            function getAge(dateString) {
                var today = new Date();
                var birthDate = new Date(dateString);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }
        });
    </script>
@endsection
