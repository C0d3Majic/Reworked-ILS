@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <style>
        select {
            display: block;
        }
        .modal{
            max-height:18%;
        }
    </style>
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->
        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s4">
                            @if(Auth::user()->role == 1)
                                <h5> Registro de Alumnos</h5>
                                <label>Select Branch</label>
                                <select id="branches" class="browser-default">
                                @foreach($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                @endforeach
                                </select>
                            @endif
                            </div>
                            {{--<div class="col s8">--}}
                                {{--<input style="font-size:20px" id="date" type="text" name="daterange"--}}
                                       {{--value="01/01/2017 - 01/31/2017"/>--}}
                            {{--</div>--}}
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="alerts" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>#Control</th>
                                            <th>Apellido</th>
                                            <th>Nombre</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($students as $student)
                                            <tr>
                                                <td>{{$student->id}}</td>
                                                <td>{{$student->controlNumber}}</td>
                                                <td>{{$student->last_name}}</td>
                                                <td>{{$student->first_name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Structure -->
        <div id="modal2" class="modal">
            <div class="modal-content">
                <h4>Eliminar curso</h4>
                <p>Estas seguro de querer eliminar este curso?</p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col s6">
                        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>
                    </div>
                    <div class="col s6">
                        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <!--
          Content Section End
        -->
@endsection
