@extends("partials.layouts.attendance_layout")

@section("content")
<style>
  html,body{
    background-color:green;
  }
.pulse-button {
  font-weight: 300;
  font-size: 0.8rem;
  color: #fff;
  background-color: #26a69a;
  border-radius: 2px;
  
  position: relative;
  border: none;
  box-shadow: 0 0 0 0 rgba(0,0,0, 0.7);
  background-color: #e84c3d;
  background-size:cover;
  background-repeat: no-repeat;
  cursor: pointer;
  -webkit-animation: pulse 1s infinite cubic-bezier(0.66, 0, 0, 1);
  -moz-animation: pulse 1s infinite cubic-bezier(0.66, 0, 0, 1);
  -ms-animation: pulse 1s infinite cubic-bezier(0.66, 0, 0, 1);
  animation: pulse 1s infinite cubic-bezier(0.66, 0, 0, 1);
}


@-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
</style>
<main>
  <container>
    <div class="card blue-white darken-1">
      <div class="card-content black-text">
        <span style="text-align:center" class="card-title"><b>Información de Pagos del Estudiante en el curso {{$course->name}} </b></span>
        <div class="row">
          <div class="col s12">
            <table id="users" class="highlight centered responsive-table">
              <thead>
                <tr>                    
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Correo</th>
                    <th>Num Control</th>
                    <th>Inscripción</th>
                    <th>Primer pago</th>
                    <th>Segundo pago</th>
                    <th>Tercer pago</th>
                </tr>
              </thead>
              <tbody>               
                <tr>                      
                  <td>{{$student->first_name}} {{$student->last_name}}</td>
                  <td>{{$student->phone}}</td>
                  <td>{{$student_payments[0]->email}}</td>               
                  <td>{{$student->controlNumber}}</td>
                <?php
                  $today = date("Y-m-d");
                  $start_date = $course->start_date;
                  for($x=0;$x<=3;$x++)
                  {
                    if(in_array($x, $payments))
                      echo '<td><span class="new badge green" data-badge-caption="saldado">Pago</span></td>';
                    else{
                      $days_to_add  = $x * 30;
                      $start_date_plus_x_months = date('Y-m-d', strtotime("+".$x." months", strtotime($start_date)));
                      if($today > $start_date_plus_x_months){
                        echo '<td><span class="new badge black pulse-button" data-badge-caption="Faltante">Pago</span></td>'; 
                        ?><script>$('html').css("background-color","red");</script>
                      <?php } 
                        
                    else
                        echo '<td><span class="new badge red pulse-button" data-badge-caption="pendiente">Pago</span></td>'; 
                    }
                  } 
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <!--div class="row">
          <div class="col s3 push-s9">
            <a id="loadMore" class="waves-effect waves-light btn">Cargar mas registros</a>
          </div>
        </div-->
      </div>
    </div>
    <div class="row">
      <div class="col s4 push-s8">
        <a id="btnRegresar" href="{{url('attendance')}}" class="waves-effect waves-light btn">Regresar a capturar asistencias</a>
      </div>
    </div>
  </container>
</main>
<script>

</script>
<!--
  Content Section End
-->
@endsection
