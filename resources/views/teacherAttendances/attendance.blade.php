@extends("partials.layouts.attendance_layout")

@section("content")
<!--
  Content Section Start
-->
<div class="col s12  z-depth-6 white lighten-4 card-panel" style="width: auto; height: auto; margin: auto; padding: auto;" >
	<form class="login-form" id="teacher_attendance_form" method="post" action="{{url('teacher_attendance')}}">
		{{ csrf_field() }}
		<div class="row">
                <P class="center">Escanea aquí tu código QRCode</P>
                <video width="320" height="240" autoplay></video>
        </div>
		<div class="row">
			<div class="input-field col s12"><i class="material-icons prefix">person</i>
				<input placeholder="Número de identificacion" id="teacherNumber" name="teacherNumber" type="number" class="validate valid">
				<label class="active" for="teacherNumber">Número de identificacion</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12"><i class="material-icons prefix">class</i>
				<select id="teacherCourses" name="teacherCourse" >
					<option value="" disabled selected>Selecciona tu curso</option>
				</select>
				<label>Curso</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12"><i class="material-icons prefix">class</i>
				<select name="entryType">
					<option value="" disabled selected>Seleccione entrada o salida</option>
					<option value="Entrada">Entrada</option>
					<option value="Salida">Salida</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col s12" style="text-align:center">
				<button class="btn waves-effect waves-light" id="btnLogin" name="Register">Capturar Asistencia </button>
			</div>
		</div>
	</form>
</div>
<!-- QCode-Decoder-------------->
    <script type="text/javascript" src="/js/qrcode.min.js"></script>
    <script>

        $(document).ready(function () {
            var submited = false;
            (function () {
                'use strict';
                var qr = new QCodeDecoder();
                if (!(qr.isCanvasSupported() && qr.hasGetUserMedia())) {
                    alert('Your browser doesn\'t match the required specs.');
                    throw new Error('Canvas and getUserMedia are required');
                }
                var video = document.querySelector('video');
                var reset = document.querySelector('#reset');
                var stop = document.querySelector('#stop');

                function resultHandler(err, result) {

                    if (err) return console.log(err.message);

                    if(!result) return

                    $('#teacherNumber').val(result);
                    $('#teacherNumber').focus();

                    var jsonObject = {
                        controlNumber : $("#teacherNumber").val()
                    }
                    // Start $.ajax() method
                    if(submited) return;
                    $.ajax({
                        // The URL for the request. variable set above
                        url: "{{url('retrieve/teacher/courses')}}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        // The data to send (will be converted to a query string). variable set above
                        data: jsonObject,
                        // Whether this is a POST or GET request
                        type: "POST",
                        // The type of data we expect back. can be json, html, text, etc...
                        dataType : "json",
                        // Code to run if the request succeeds;
                        // the response is passed to the function
                        success: function( msg ) {
                            if(msg.status == 'error'){
                                var $toastContent = $('<span>'+ msg.type +'</span>');
                                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                            }
                            else if(msg.status == 'success')
                            {
                                if(msg.courses.length==1)
                                {
                                    $('#teacherCourses').empty();
                                    $('#teacherCourses').append('<option selected value="' + msg.courses[0].id + '">' + msg.courses[0].name + '</option>');

                                    submited = true;
                                    $("#teacher_attendance_form").submit();
                                }
                                else {
                                    console.log(msg.courses);
                                    $('#teacherCourses').empty();
                                    $('#teacherCourses').append('<option value="" disabled selected>Selecciona tu curso</option>');
                                    for (i = 0; i < msg.courses.length; i++)
                                        $('#teacherCourses').append('<option value="' + msg.courses[i].id + '">' + msg.courses[i].name + '</option>');
                                }
                                $('select').material_select();
                            }
                        },
                        error: function(){
                            var $toastContent = $('<span>There was an error on the server</span>');
                            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }
                    });
                    return false;
                }

                // prepare a canvas element that will receive
                // the image to decode, sets the callback for
                // the result and then prepares the
                // videoElement to send its source to the
                // decoder.
                qr.decodeFromCamera(video, resultHandler);
                // attach some event handlers to reset and
                // stop whenever we want.

                // reset.onclick = function () {
                //     qr.decodeFromCamera(video, resultHandler);
                // };
                // stop.onclick = function () {
                //     qr.stop();
                // };

            })();
        });

    </script>
    <!--
      Content Section End
    -->
@endsection