@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Registrar una asistencia de un profesor</span>
                        <div class="row">
                            <form name="form1" class="col s12" method="POST" class="form-horizontal" role="form"
                                  action="{{ action('TeacherAttendancesController@store') }}" }}
                            ">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="row">
                                <div style="display: visible">
                                    <table id="teachers" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            {{--<th>Registro</th>--}}
                                            <th>Apellido</th>
                                            <th>Nombre</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($teachers as $teacher)
                                            <tr>
                                                <td>{{$teacher->id}}</td>
                                                {{--<td>{{$teacher->registration_date}}</td>--}}
                                                <td>{{$teacher->last_name}}</td>
                                                <td>{{$teacher->first_name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: visible">
                                    <table id="courses" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Lenguaje</th>
                                            <th>Nivel</th>
                                            <th>Profesor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($courses as $course)
                                            <tr>
                                                <td>{{$course->id}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->language->name }}</td>
                                                <td>{{$course->level}}</td>
                                                <td>{{$course->teacher->last_name.' '.$course->teacher->first_name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <input name="attendance_date" id="icon_prefix" type="text" class="datepicker"
                                           value="">
                                    <label for="attendance_date">Fecha en la que se registra la asistencia</label>
                                </div>
                                <div class="input-field col s4">
                                    <input name="attendance_time" id="icon_prefix" type="text" class="timepicker"
                                           value="">
                                    <label for="attendance_time">Hora en la que se registra la asistencia</label>
                                </div>
                                <div class="input-field col s4">
                                    <select name="type" class="browser-default">
                                        <option value="" selected>Seleccione entrada o salida</option>
                                        <option value="Entrada">Entrada</option>
                                        <option value="Salida">Salida</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="selectedTeachersIds" id="selectedTeachersIds" value=""/>
                            <input type="hidden" name="selectedCoursesIds" id="selectedCoursesIds" value=""/>
                            <div class="row">
                                <div class="input-field col s4">
                                    <button class="btn-block btn-large waves-effect waves-light green"
                                            type="submit">Guardar
                                        <i class="material-icons right">check</i>
                                    </button>
                                </div>
                                <div class="col s10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            var table1 = $('#teachers').DataTable({
                select: true,
                select: {
                    style: 'single'
                }
            });

            var table2 = $('#courses').DataTable({
                select: true,
                select: {
                    style: 'single'
                }
            });

            table1
                .on('select', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table1.rows({selected: true}).data().toArray();

                    console.log('holaaa');

                    $('#selectedTeachersIds').val('abc');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedTeachersIds').val(rowData[i][0]);
                        else
                            $('#selectedTeachersIds').val($('#selectedTeachersIds').val() + ',' + rowData[i][0]);
                    }
                    console.log($('#selectedTeachersIds').val());
                    /*****/
                    var jsonObject = {
                        teacher_id : $('#selectedTeachersIds').val()
                    }
                    // Start $.ajax() method
                    $.ajax({
                        // The URL for the request. variable set above
                        url: "{{url('retrieve/teacher/courses/byid')}}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        // The data to send (will be converted to a query string). variable set above
                        data: jsonObject,
                        // Whether this is a POST or GET request
                        type: "POST",
                        // The type of data we expect back. can be json, html, text, etc...
                        dataType : "json",
                        // Code to run if the request succeeds;
                        // the response is passed to the function
                        success: function( msg ) {
                            if(msg.status == 'error'){
                                table2.rows().remove();
                                table2.rows().draw();
                                var $toastContent = $('<span>'+ msg.type +'</span>');
                                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                            }else if(msg.status == 'success')
                            {
                                console.log(msg.courses);
                                table2.rows().remove();
                                table2.rows().draw();
                                for(i=0; i<msg.courses.length; i++)
                                    table2.row.add([msg.courses[i].id,msg.courses[i].name,msg.courses[i].language,msg.courses[i].level,msg.courses[i].teacher]);
                                table2.rows().draw();
                            }
                        },
                        error: function(){
                        var $toastContent = $('<span>There was an error on the server</span>');
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                        }
                    });
                    return false;

                    /****/
                })

            table1
                .on('deselect', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table1.rows({selected: true}).data().toArray();

                    $('#selectedTeachersIds').val('');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedTeachersIds').val(rowData[i][0]);
                        else
                            $('#selectedTeachersIds').val($('#selectedTeachersIds').val() + ',' + rowData[i][0]);
                    }
                })

            table2
                .on('select', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table2.rows({selected: true}).data().toArray();

                    $('#selectedCoursesIds').val('');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedCoursesIds').val(rowData[i][0]);
                        else
                            $('#selectedCoursesIds').val($('#selectedCoursesIds').val() + ',' + rowData[i][0]);
                    }
                })

            table2
                .on('deselect', function (e, dt, type, indexes) {
                    if (type == 'row')
                        var rowData = table2.rows({selected: true}).data().toArray();

                    $('#selectedCoursesIds').val('');
                    for (var i = 0; i < rowData.length; i++) {
                        if (i == 0)
                            $('#selectedCoursesIds').val(rowData[i][0]);
                        else
                            $('#selectedCoursesIds').val($('#selectedCoursesIds').val() + ',' + rowData[i][0]);
                    }
                })

            $('.datepicker').daterangepicker(
                {
                    singleDatePicker: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    },
                },
                function (start, end, label) {
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });

            $('.timepicker').timepicker({'timeFormat': 'H:i'});
        });

        $("#createmenu").attr("href", "/teacherAttendances/create");
        $("#listmenu").attr("href", "/teacherAttendances");

    </script>
@endsection
