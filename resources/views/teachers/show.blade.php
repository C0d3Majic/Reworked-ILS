@extends("partials.layouts.crud_layout")


@section("content")
    <!--
  Content Section Start
-->
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu")
    <!--- Right side of the view -->

        <div class="col s10">
            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Ver profesor</span>
                        <div class="row">
                            <form action="{{ route('TeachersController.update', $teacher->id) }}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <div class="row">
                                    <div class="input-field col s4 right">
                                        <input name="registration_date" id="icon_prefix" type="text" class="timepicker"
                                               value={{ $teacher->registration_date }}>
                                        <label for="registration_date">Fecha de Registro</label>
                                    </div>
                                </div>
                                <h5 style="text-align: center">Datos del profesor</h5>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <input name="last_name" id="icon_prefix" type="text" value={{ $teacher->last_name }}>
                                        <label for="last_name">Apellido(s)</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="first_name" id="icon_prefix" type="text" value={{ $teacher->first_name }}>
                                        <label for="first_name">Nombre(s)</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <input name="controlNumber" id="icon_prefix" type="number" value="{{ $teacher->controlNumber }}">
                                        <label for="controlNumber">Matricula</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s8">
                                        <input name="address" id="icon_prefix" type="text" value={{ $teacher->address }}>
                                        <label for="address">Dirección</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input name="telephone1" id="icon_prefix" type="text" value={{ $teacher->telephone1 }}>
                                        <label for="telephone1">Teléfono1</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input name="telephone2" id="icon_prefix" type="text" value={{ $teacher->telephone2 }}>
                                        <label for="telephone2">Teléfono2</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="RFC" id="icon_prefix" type="text" value={{ $teacher->RFC }}>
                                        <label for="RFC">RFC</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select multiple id="languages" name="languages[]">
                                            <option value="" disabled selected>Revisar seleccionados</option>
                                            <?php
                                            $array_of_days  =   explode(',', $teacher->known_languages);
                                            $y=0;
                                            foreach($languages as $language)
                                            {
                                                if($y<count($array_of_days) && $language->id == $array_of_days[$y])
                                                {
                                                    echo '<option value='.$language->id.' selected>'.$language->name.'</option>';
                                                    $y++;
                                                }else
                                                    echo '<option value='.$language->id.'>'.$language->name.'</option>';
                                            }
                                            ?>
                                        </select>
                                        <label for="known_languages" class="active">Lenguajes que domina</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s3">
                                        <input name="salary_per_hour" id="icon_prefix" type="text" value={{ $teacher->salary_per_hour }}>
                                        <label for="salary_per_hour">Salario por hora</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="current_checkin" id="icon_prefix" type="text" value={{ $teacher->current_checkin }}>
                                        <label for="current_checkin">Hora de entrada</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input name="current_checkout" id="icon_prefix" type="text" value={{ $teacher->current_checkout }}>
                                        <label for="current_checkout">Hora de salida</label>
                                    </div>
                                </div>
                                <div class="row">
                                    {{--<div class="input-field col s4">--}}
                                        {{--<button class="btn-block btn-large waves-effect waves-light green"--}}
                                                {{--type="submit">Actualizar--}}
                                            {{--<i class="material-icons right">check</i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    <div class="col s10">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $("input, textarea, select").prop("readonly", true);
            $('select').material_select();
        });

        $("#createmenu").attr("href", "/teachers/create");
        $("#listmenu").attr("href", "/teachers");

    </script>
@endsection
