@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    @include("partials.ajax.alerts_ajax")
    <div class="container">
      <!-- Left side of the view AKA THE CRUD MENU -->
      @include("partials.crudmenu2")
      <!--- Right side of the view -->
            <div class="col s10">

                <div class="col s12" style="padding: 0.5% 1%;">
                    <div class="card z-depth-4 grey lighten-5">
                        <div class="card-content">
                            <span class="card-title">Editar usuario</span>
                            <div class="row">
                                <div class="row">
                                    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id],'class'=> 'col s12', 'role'=>'form']) !!}
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::text('name', $user->name, array('style' => 'font-size:15px;')) }}
                                            {{ Form::label('name', 'nombre', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::text('email', $user->email, array('style' => 'font-size:15px;')) }}
                                            {{ Form::label('email', 'correo electrónico', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::text('name_tag', $user->name_tag, array('style' => 'font-size:15px;')) }}
                                            {{ Form::label('name_tag', 'id', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                        </div>
                                    </div>
                                    {{--<br>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="input-field col s4">--}}
                                            {{--{!! Form::select('role', $roles, null, ['style' => 'font-size:15px;']) !!}--}}
                                            {{--{!! Form::Label('role', 'Selecciona el rol del usuario', array('style' => 'font-size:15px;', 'class' => 'active')) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::text('new_password', '', array('style' => 'font-size:15px;')) }}
                                            {{ Form::label('new_password', 'New Passowrd', array('style' => 'font-size:15px;', 'class' => 'active')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="input-field col s4">
                                            {{ Form::submit('Actualizar', array('class' => 'btn-block btn-large waves-effect waves-light green')) }}
                                        </div>
                                    </div>
                                    {{ Form::Close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#createmenu").attr("href", "/users/create");
        $("#listmenu").attr("href", "/users");
    </script>
@endsection
