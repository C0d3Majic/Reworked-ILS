@extends("partials.layouts.crud_layout")

@section("content")
    <!--
  Content Section Start
-->
    <style>
        select {
            display: block;
        }
    </style>
    <div class="container">
        <!-- Left side of the view AKA THE CRUD MENU -->
    @include("partials.crudmenu2")
    <!--- Right side of the view -->
        <div class="col s10">

            <div class="col s12" style="padding: 0.5% 1%;">
                <div class="card z-depth-4 grey lighten-5">
                    <div class="card-content">
                        <span class="card-title">Listado de usuarios</span>
                        <div class="row">
                            <div class="col s12">
                                <div id="welcome" class="black-text">
                                    <table id="parts" class="responsive-table highlight black-text">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Correo electrónico</th>
                                            <th>Rol</th>
                                            {{--<th>Gaffete</th>--}}
                                            {{--<th>Departamento</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->role}}</td>
                                                {{--<td>{{$user->name_tag}}</td>--}}
                                                {{--<td>{{$user->departament}}</td>--}}
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /*Ajax funtions for welcome page*/
        $(document).ready(function () {
            $selectedId = 0;
            var parts = $('#parts').DataTable({
                select: true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                    "select": {
                        rows: " / %d selecionado(s)"
                    },
                },
                "order": [[0, "desc"]],
                responsive: true,
                "columnDefs": [
                    {"width": "10%", "targets": 0}
                ]
            });

            parts
                .on('select', function (e, dt, type, indexes) {
                    var rowData = parts.rows(indexes).data().toArray();
                    $selectedId = rowData[0][0]
                    $("#editmenu").attr("href", "/users/" + $selectedId + "/edit");
                    $("#showmenu").attr("href", "/users/" + $selectedId );
                })

            var table = $('#alerts').DataTable();
            $("#deletemenu").click(function () {
                bootbox.confirm({
                    message: "Estas seguro que deseas eliminar este registro de la base de datos?",
                    backdrop: false,
                    size: "small",
                    buttons: {
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-danger'
                        },
                        confirm: {
                            label: 'Eliminar',
                            className: 'btn-success'
                        },
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                url: '/users/' + $selectedId,
                                type: 'DELETE',
                                data: {'_token': '{!! csrf_token() !!}'},
                                async: false,
                                success: function (result) {
                                    console.log(result);
                                    if (result == "success") {
                                        setTimeout(function () {// wait for 1 secs(2)
                                            location.reload();
                                        }, 1000);
                                    }
                                }
                            });
                        }
                    }
                });
            });

        });
    </script>
    <script>
        $("#createmenu").attr("href", "/users/create");
        $("#listmenu").attr("href", "/users");
    </script>
@endsection
