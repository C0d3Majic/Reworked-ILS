<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Task;
use Illuminate\Http\Request;

Route::get('/home', function () {
    return view('welcome');
});
//Route::get('teachersCtrl', 'SiteController@setAutoCtrlNumber'); custom route to update auto generated ctrl numbers

/*---Added routes by CodeMajic */
Route::get('attendance','SiteController@load_view_attendance');
Route::post('attendance', 'StudentAttendancesController@record_StudentAttendance');
Route::get('teacher_attendance','SiteController@load_view_teacher_attendance');
Route::post('teacher_attendance', 'TeacherAttendancesController@record_TeacherAttendance');
Route::get('students/attendance_report/{course_id}','SiteController@student_attendance_report');
Route::get('students/attendance_report/{course_id}','SiteController@student_new_report');
Route::get('teachers/attendance_report/{course_id}','SiteController@teacher_attendance_report');
Route::get('student/data/{student_id}', 'SiteController@student_data_form');
Route::get('teacher/credential/{teacher_id}', 'SiteController@teacher_credential');
Route::get('billing/payment_report/{course_id}','SiteController@payments_report');
Route::get('grade/payment_report/{course_id}','SiteController@grades_report');
Route::post('retrieve/teachers','SiteController@retrieve_teachers_from_language');
Route::post('retrieve/student/courses', 'SiteController@retrieve_student_courses');
Route::post('retrieve/teacher/courses', 'SiteController@retrieve_teacher_courses');
Route::post('retrieve/student/courses/byid', 'SiteController@retrieve_student_courses_byid');
Route::post('retrieve/teacher/courses/byid', 'SiteController@retrieve_teacher_courses_byid');
Route::get('student/payment/info/{controlNumber}/{course_id}', 'SiteController@retrieve_student_payment_info');
Route::get('calendar','SiteController@show_calendar_view');
Route::get('course/lenght/{course_id}', 'SiteController@course_lenght');
Route::post('course/retrieve','CoursesController@retrieve_course');
Route::post('language/retrieve','LanguagesController@retrieve_language');
Route::post('teacher/retrieve','TeachersController@retrieve_teacher');
Route::post('student/retrieve','StudentsController@retrieve_student');
Route::post('branch/retrieve','BranchesController@retrieve_branch');
Route::post('students/change/branch', 'StudentsController@change_branch');
Route::post('courses/change/branch', 'CoursesController@change_branch');
Route::post('teachers/change/branch', 'TeachersController@change_branch');
Route::post('languages/change/branch', 'LanguagesController@change_branch');
Route::post('course/edit', 'CoursesController@edit_description');
Route::post('course/description/save_edit', 'CoursesController@save_edit_description');
/*---- End of routes Added     */

Route::get('students','StudentsController@index');
Route::post('students/retrieve', 'StudentsController@retrieve_alerts');
Route::post('students/courses', 'StudentsController@assign_courses');
Route::get('students/create', 'StudentsController@create');
Route::post('students', 'StudentsController@store');
Route::get('students/{id}/edit', 'StudentsController@edit');
Route::put('{id}', 'StudentsController@update')->name('StudentsController.update');
Route::delete('students/{id}', 'StudentsController@destroy');
Route::get('students/{id}', 'StudentsController@show');

Route::resource('teachers', 'TeachersController');
Route::put('teachers/{id}/edit', 'TeachersController@update')->name('TeachersController.update');

Route::resource('courses', 'CoursesController');
Route::put('courses/{id}/edit', 'CoursesController@update')->name('CoursesController.update');

Route::resource('languages', 'LanguagesController');
Route::put('languages/{id}/edit', 'LanguagesController@update')->name('LanguagesController.update');

Route::resource('studentAttendances', 'StudentAttendancesController');
Route::put('studentAttendances/{id}/edit', 'StudentAttendancesController@update')->name('StudentAttendancesController.update');

Route::resource('teacherAttendances', 'TeacherAttendancesController');
Route::put('teacherAttendances/{id}/edit', 'TeacherAttendancesController@update')->name('TeacherAttendancesController.update');

Route::resource('users', 'UsersController');

Route::resource('billing', 'BillingController');

Route::resource('grades', 'GradesController');

Route::resource('branches', 'BranchesController');
Route::put('branches/{id}/edit', 'CoursesController@update')->name('CoursesController.update');

Route::resource('log', 'LogController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});
